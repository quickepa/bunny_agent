import pickle
import random
from collections import namedtuple, deque
from typing import List
import time
import torch as T
import numpy as np

import events as e

from .models.dql import singleton

# Save model to
#model_file_path_save = './saves/dql/crates-path-features-advanced.pt'
#model_file_path_save = './saves/dql/fighter.pt'
model_file_path_save = './saves/dql/final_'



# Events
PLACEHOLDER_EVENT = "PLACEHOLDER"

def setup_training(self):
    """
    Initialise self for training purpose.

    This is called after `setup` in callbacks.py.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    """
    # Example: Setup an array that will note transition tuples
    # (s, a, r, s')

    # Activate CUDA if available
    # self.model.network.device = T.device('cuda:0' if T.cuda.is_available() else 'cpu')
    # self.model.network.to(self.model.network.device)
    #singleton.model.network.device = T.device('cuda:0' if T.cuda.is_available() else 'cpu')
    #singleton.model.network.to(singleton.model.network.device)
    singleton.model.prepare_training(self.agent_name)
    return


def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    """
    Called once per step to allow intermediate rewards based on game events.

    When this method is called, self.events will contain a list of all game
    events relevant to your agent that occurred during the previous step. Consult
    settings.py to see what events are tracked. You can hand out rewards to your
    agent based on these events and your knowledge of the (new) game state.

    This is *one* of the places where you could update your agent.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    :param old_game_state: The state that was passed to the last call of `act`.
    :param self_action: The action that you took.
    :param new_game_state: The state the agent is in now.
    :param events: The events that occurred when going from  `old_game_state` to `new_game_state`
    """
    self.logger.debug(f'Encountered game event(s) {", ".join(map(repr, events))} in step {new_game_state["step"]}')

    # Idea: Add your own events to hand out rewards
    #if ...:
    #    events.append(PLACEHOLDER_EVENT)

    # state_to_features is defined in callbacks.py
    #self.model.append_transition(old_game_state, self_action, new_game_state, events)
    singleton.model.append_transition(old_game_state, self_action, new_game_state, events, self.agent_name)
    # Do PER-MOVE training here
    #self.model.train_per_move()
    singleton.model.train_per_move(self.agent_name)


def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    """
    Called at the end of each game or when the agent died to hand out final rewards.

    This is similar to reward_update. self.events will contain all events that
    occurred during your agent's final step.

    This is *one* of the places where you could update your agent.
    This is also a good place to store an agent that you updated.

    :param self: The same object that is passed to all of your callbacks.
    """
    self.logger.debug(f'Encountered event(s) {", ".join(map(repr, events))} in final step')
    #self.model.append_transition(last_game_state, last_action, None, events)
    singleton.model.append_transition(last_game_state, last_action, None, events, self.agent_name)
    
    # Do PER-EPISODE training here
    start_time = time.perf_counter()
    #self.model.train()
    singleton.model.train(self.agent_name)

    self.logger.debug(f'Model training done. It took {time.perf_counter() - start_time} s')
    
    # THIS IS A GIGANTIC BOTTLENECK!
    # Store the model
    if last_game_state['round'] % 500 == 0:
        self.logger.debug(f"Model saving in episode {last_game_state['round']}")
        start_time = time.perf_counter()
        round_string = str(last_game_state['round'])
        singleton.model.save(model_file_path_save + round_string + '.pt', self.agent_name)
        self.logger.debug(f'Model saving done. It took {time.perf_counter() - start_time} s')

        # # Deactivate CUDA, so we can use models trained on GPU on other computers
        # self.model.network.device = T.device('cpu')
        # self.model.network.to(self.model.network.device)
        # self.logger.debug(f"Model saving in episode {last_game_state['round']}")
        # start_time = time.perf_counter()
        # with open(model_file_path_save, "wb") as file:
        #     pickle.dump(self.model, file)
        # self.logger.debug(f'Model saving done. It took {time.perf_counter() - start_time} s')
        # np.save(self.model.score_file, self.model.scores)
        # np.save(self.model.loss_file, self.model.loss)
        # # Activate CUDA if available
        # self.model.network.device = T.device('cuda:0' if T.cuda.is_available() else 'cpu')
        # self.model.network.to(self.model.network.device)

#def enemy_game_events_occurred(self, enemy_name: str, old_enemy_game_state: dict, enemy_action: str, enemy_game_state: dict, enemy_events: List[str]):
#    return