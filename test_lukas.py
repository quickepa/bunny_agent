import numpy as np
import scipy.linalg as scl
#from skimage.util.shape import view_as_windows


gamma = 0.95

def nstepTD(R, Q_tplus, n):
    M = len(R)
    gamma_ = np.power(gamma, np.arange(0, n))
    row = np.pad(gamma_, (0, M-n), 'constant')
    col = np.zeros((M))
    col[0] = 1
    gamma_ = scl.toeplitz(col, row)
    print("GAMMA: ", gamma_)
    R_ = np.repeat(np.reshape(R, (1, M)), M, axis = 0)
    print(R_)
    R_nstep=np.diag(np.dot(R_, gamma_.T))
    print("R_nstep:" , R_nstep)
    Q_n = np.concatenate([np.max(Q_tplus, axis=1)[n:], np.zeros(n)])
    print(Q_n)
    y = R_nstep + np.power(gamma, n)*Q_n
    return y


M = 10
R = np.ones((M))
Q_tplus = np.full((M, 6), 2)
n = 1
td = nstepTD(R, Q_tplus, n)

print("R:")
print(R)
print("Q_tplus:")
print(Q_tplus)
print("td:")
print(td)


'''
a = np.ones((5, 2))
a = np.pad(a, ((0, 9-len(a)), (0, 0)), 'constant')
print(a)

def sliding_windows_vw(a, W):
    a = np.asarray(a)
    p = np.zeros(W-1,dtype=a.dtype)
    b = np.concatenate((p,a,p))
    return view_as_windows(b,len(a)+W-1)[::-1]

M = 10
n = 3
gamma = 0.95
R = np.ones((M))
Q = np.ones((M, 6))

gamma_ = np.power(gamma, np.arange(0, n))
row = np.pad(gamma_, (0, M-n), 'constant')
col = np.zeros((M))
col[0] = 1
gamma_ = scl.toeplitz(col, row)
print(gamma_.T)
R_ = np.repeat(np.reshape(R, (1, M)), M, axis = 0)
print(R_)
print( np.diag(np.dot(R_, gamma_.T)) )
'''
'''
a = np.array([1, 2, 3, 4])
aa = np.pad(a, (0, n), 'constant')
print(a)
print(aa)
'''
'''
ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']
A = ['BOMB', 'UP', 'DOWN']
idx = np.array([ACTIONS.index(a) for a in A])

def get_one_hot(targets, nb_classes):
        res = np.eye(nb_classes)[np.array(targets).reshape(-1)]
        return res.reshape(list(targets.shape)+[nb_classes])

a = np.outer(np.arange(0, 3), np.arange(0, 3))
aaa = np.tile(a, (len(ACTIONS), 1, 1))

#b = get_one_hot(idx, len(ACTIONS))#[:, :, np.newaxis]
b = np.eye(len(ACTIONS))
bbb = np.repeat(b, np.repeat(len(a), len(ACTIONS)), axis=0).reshape((6, 3, 6))

c = np.concatenate([aaa, bbb], -1)
print(b)
print(bbb)
print(a)
print(aaa)

print(aaa.shape)
print(bbb.shape)

print(c)
'''