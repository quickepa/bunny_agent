import pickle
import os

# @ TODO, IF NEEDED IMPLEMENT MODEL SHRINKING!!

# If a model is available, use it
if os.path.isfile(model_file_path_load):
    self.logger.info("Loading model from saved state.")
    with open(model_file_path_load, "rb") as file:
        self.model = pickle.load(file)