import numpy as np

from ..IBrain import IBrain
from ..utility import ACTIONS

class RandomBrain(IBrain):
    """
    Model class that is used, if no other Model can be loaded and training mode is off
    i.e. the back-up model
    Since it only interfaces with 'callbacks.py' by way of a policy() call,
    it only implements this function of IModel.
    """
    def __init__(self):
        #super.__init__(self)
        pass

    def policy(self, state):
        """
        The policy that is called in 'callbacks.py' in 'act()'.
        Samples action from uniform distribution.

        Parameters
        ----------
        state : dict
            State on which to predict optimal action

        Returns
        -------
        String
            Uniformly random sampled action.
        """
        return np.random.choice(ACTIONS, p=[1./6, 1./6, 1./6, 1./6, 1./6, 1./6])