import numpy as np
from collections import deque
import random
import scipy.linalg as scl

from ..IBrain import IBrain
from ..utility import state_to_features, reward_from_events, ACTIONS, Transition
from ..policy import softmax_policy, eps_greedy_policy

class LinearBrain(IBrain):
    """
    Linear Model
    """
    def __init__(self, agent_reference):
        #super.__init__()
        # keep reference to agent.self for logging and stuff
        self.agent_reference = agent_reference 

        # Transition Buffer stuff
        # TODO: Maybe do a list of transitions to hold
        # history of each episode?
        self.transition_buffer_size = 200000 # max buffer size
        self.n_transitions = 0 # number of transitions currently in the buffer
        self.transitions = deque(maxlen=self.transition_buffer_size)

        # Training runs
        self.n_training_rounds = 0

        # @TODO: beta shape needs length of feature vector
        self.beta = np.zeros( (len(ACTIONS), 33) )

        # HYPERPARAMETERS
        # TODO: Find good values
        self.gamma = 0.95 # Discount for TD / SARSA
        self.n_steps = 5 # Number of steps for n-step TD
        
        self.eps = 0.0 # epsilon for eps_greed_policy()
        self.rho = 0.001 # temperature for softmax_policy()

        self.learning_rate = 1e-8 # alpha for beta updates


    def train(self):
        """
        Training method called in 'train.py' after each EPISODE in 'end_of_round()'.
        Operates on data recorded in transtions.
        We train one linear model per action. The update for model A is done after each
        move of the agent.

        Other possibilities to train:
            - record a few actions (e.g. one epsisode worth) and train on the batch
        Other things:
            - Store transitions per action, so we dont have to sort them?
            - Store transitions per episode and train on multiple episodes, to fit on bigger batch
        Returns
        -------
        """
        #nothing to train on
        if len(self.transitions) == 0:
            return 
        # Get rid of first entry, since 'old_state' and 'action' are 'None'
        #self.transitions.popleft()
        # Retrieve lists from the Deque of tuples
        old_states, actions, new_states, rewards = zip(*self.transitions)
        # Clear the queue since we only train on the states of each batch
        self.transitions.clear()
        self.n_transitions = 0

        # Format for numpy goodness
        X = np.array(old_states)
        R = np.array(rewards)
        A = np.array(actions)

        # @TODO: This is a hotfix, please do this nicer!
        #if self.n_training_rounds==0:
        #    self.beta = np.zeros((len(ACTIONS), X.shape[1]))

        # Compute expected return = response 
        Q_tplus = self.estimate_all_actions(X)
        y = R + self.gamma * np.concatenate([ np.max(Q_tplus[1:], axis=1), [0]])
        #y = self.nstepTD(R, Q_tplus, self.n_steps)
        
        # Update the linear models
        # @TODO this can surely be vectorized
        for a, action in enumerate(ACTIONS):
            mask = np.where(A == a)
            if mask[0].shape[0] == 0: # if this action has not been performed in this batch, skip
                continue
            Xa = X[mask]
            ya = y[mask]
            #print("LENGHT OF Xa: ", len(Xa))
            #print("INVALID VALUES: ", self.beta[a])
            #self.beta[a] = self.beta[a] + self.learning_rate/len(Xa)*np.sum(np.dot(Xa.T, ya - np.dot(Xa, self.beta[a])), axis=0)
            update = np.sum(np.multiply(Xa.T, (ya - np.dot(Xa, self.beta[a]))), axis=1)
            #print("Xa: ", Xa.shape)
            #print("Y: ", np.multiply(Xa.T, (ya - np.dot(Xa, self.beta[a]))).shape)
            #print("UPDate: ", update.shape)
            self.beta[a] = self.beta[a] + self.learning_rate/len(Xa)*update#np.sum(update, axis=1)

        # Housekeeping
        self.n_training_rounds += 1
        return


    def estimate_state_action_pairs(self, X, A):
        """
        For all state-actions-pairs, compute Q-values
        i.e. Q(x, a) for (x, a) in (X, A])
        If no model has been fitted yet, return vector of 0's.

        Parameters
        ----------
        X : np.array shape=(#States, #Features)
            State feature matrix
        A : np.array
            Actions (encoded as 0...5) which were performed in the corresponding states

        Returns
        -------
        np.array shape=(#States)
            Q estimates for all pairs (x, a) in (X, A)
        """

        Q = np.zeros(len(X))
        for a, action in enumerate(ACTIONS):
            mask = np.where(A == a)
            # If there are no instances with this action, skip prediction
            if mask[0].shape[0] == 0:
                continue
            else:
                Q[mask] = np.dot(X[mask], self.beta[a])
        return Q


    def estimate_all_actions(self, X):
        """
        For state feature vector (row) in Feature matrix X, compute
        Q-value for all possible actions
        i.e. Q(x, a) for a in ACTIONS, x fixed
        If no model has been fitted yet, return vector of 0's.

        Parameters
        ----------
        X : np.array shape=(#States, #Features)
            State feature matrix

        Returns
        -------
        np.array shape=(#States, #Actions)
            Q estimates for all states and all possible actions
        """
        Q = np.zeros((len(X), len(ACTIONS)))
        for a, action in enumerate(ACTIONS):
            Q[:, a] = np.dot(X, self.beta[a])
        return Q

    
    def policy(self, state):
        """
        The policy that is called in 'callbacks.py' in 'act()'.
        Predict the Value function with current model and decide
        on an action according to some policy.

        Parameters
        ----------
        state : dict
            State on which to predict optimal action

        Returns
        -------
        String
            Optimal Action according to the policy
        """
        Q_actions = self.estimate_all_actions(self.state_to_features(state)[np.newaxis, :]).flatten()
        #return eps_greedy_policy(Q_actions, self.eps)
        return softmax_policy(Q_actions, self.rho)
        

    def append_transition(self, old_game_state, self_action, new_game_state, events):
        """
        Append new transition to the transtion buffer.
        Called in 'game_events_occurred()' and 'end_of_round()'.

        Parameters
        ----------
        old_game_state : dict
            The passed game state
        self_action : string
            The recorded action
        new_game_state : dict
            The game state that followed the action
        events : List[str]
            Events that occured during the transition
        """
        if self_action == None:
            return
        self.transitions.append(
            (Transition(self.state_to_features(old_game_state), ACTIONS.index(self_action), self.state_to_features(new_game_state), self.reward_from_events(events))))
        self.n_transitions +=1

    
    def state_to_features(self, game_state: dict):
        """
        Translate a game state into features.
        Currently a Wrapper for the utility function 'state_to_features()'
        in case we want to try different features for different models.

        Parameters
        ----------
        game_state : dict
            game state to translate

        Returns
        -------
        np.array
            The feature vector.
        """
        return state_to_features(game_state)

    
    def reward_from_events(self, events):
        """
        Compute rewards from events.
        Currently a Wrapper for the utility function 'reward_from_events()'
        in case we want to try different rewards for different models.

        Parameters
        ----------
        game_state : dict
            game state to translate

        Returns
        -------
        np.array
            The feature vector.
        """
        return reward_from_events(self.agent_reference, events)

    
    def nstepTD(self, R, Q_tplus, n):
        M = len(R)
        gamma_ = np.power(self.gamma, np.arange(0, n))
        row = np.pad(gamma_, (0, M-n), 'constant')
        col = np.zeros((M))
        col[0] = 1
        gamma_ = scl.toeplitz(col, row)
        R_ = np.repeat(np.reshape(R, (1, M)), M, axis = 0)
        R_nstep=np.diag(np.dot(R_, gamma_.T))
        Q_n = np.concatenate([np.max(Q_tplus, axis=1)[n:], np.zeros(n)])
        y = R_nstep + np.power(self.gamma, n)*Q_n
        return y
