import numpy as np

from .utility import ACTIONS

def softmax_policy(Q_actions, temp, return_probabilities=False):
    """
    Choose action according to softmax of Q-function,
    given a Estimator for Q  and a state feature vector of length D.
    
    Parameters
    ----------
    Q_actions : np.array shape=(len(ACTIONS))
        Q values for a single state and all actions
    temp : float
        Temperature of softmax function
    Returns
    -------
    string
        The chosen action
    """
    #print("Q softmax: ", Q_actions)
    exp_u = np.exp(Q_actions/temp)
    #print("exp_u softmax: ", exp_u)
    p = exp_u / np.sum(exp_u)
    #self.logger.info(f"probabilities are {p}")
    if return_probabilities:
        return np.random.choice(ACTIONS, p=p), p
    else:
        return np.random.choice(ACTIONS, p=p)

def eps_greedy_policy(Q_actions, eps):
    """
    Choose action according to epsilon greedy criterion,
    given a QEstimator and a state feature vector of length D.
        
    Parameters
    ----------
    Q_actions : np.array shape=(len(ACTIONS))
        Q values for a single state and all actions
    eps : float
        Probability of choosing a uniformly random action
        instead of following the Q-function.
    Returns
    -------
    string
        The chosen action
    """
    if random.random() < eps:
        return np.random.choice(ACTIONS, p=[1./6, 1./6, 1./6, 1./6, 1./6, 1./6])
    a = np.argmax(Q_actions)
    return ACTIONS[a]