class IBrain():
    """
    Interface for model class for 
        - estimating and revising Q(s, a) for reinforcment learning
        - returnin actions according to some policy
    Interface used in 'train.py' and 'callbacks.py'
    """
    def train(self):
        """
        Training method called in 'train.py' after each move/episode/every so often
        in 'game_events_occurred()' or 'end_of_round()'.
        Operates on data recorded in transtions.
        """
        raise NotImplementedError('train not implemented')
    
    
    def policy(self, state):
        """
        The policy that is called in 'callbacks.py' in 'act()'.

        Parameters
        ----------
        state : dict
            State on which to predict optimal action

        Returns
        -------
        String
            Optimal Action according to the policy
        """
        raise NotImplementedError('policy not implemented')


    def append_transition(self, old_game_state, self_action, new_game_state, events):
        """
        Append new transition to the transtion buffer.
        Called in 'game_events_occurred()' and 'end_of_round()'.

        Parameters
        ----------
        old_game_state : dict
            The passed game state
        self_action : string
            The recorded action
        new_game_state : dict
            The game state that followed the action
        events : List[str]
            Events that occured during the transition
        """
        raise NotImplementedError('append_transtion not implemented')

    def train_per_move(self):
        """
        Training method called in 'train.py' after each move
        in 'game_events_occurred()'.
        Operates on data recorded in transtions.
        """
        # pass since not all models will implement this
        pass

    """
    def init_transitions(self):
        '''
        Initialize the transition buffer. The child class decides,
        if this is a deque, list... and how big it is.
        Operates only on member data of the Model.
        Called in 'train.py' during 'setup_training()'
        '''
        raise NotImplementedError('init_transitions not implemented')

    def save(self, filename):
        '''
        Saves the model to file
        '''
        pass
    
    def load(self, filename):
        '''
        Loads model from file
        '''
        pass

    def predict(self):
        '''
        Prediction method called ...
        '''
        raise NotImplementedError('predict not implemented')

    def state_to_features(self, state):
        '''
        Translate game state into feaures
        '''
        raise NotImplementedError('state_to_features not implemented')
    
    def reward_from_events(self, events: List[str]):
        '''
        Compute rewards from events
        '''
        raise NotImplementedError('reward_from_events not implemented')
    """