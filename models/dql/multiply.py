import numpy as np

def mirror_fields_horizontal(X):
    """
    See mirror_matrices in utility.py
    removes bool mirror_horizontally. Instead there are two separate functions
    """
    # vertical middle line fixed for square matrices of odd shape
    return np.pad(X,((0,0),(0,0),(X.shape[1]-1,0)),'reflect')[:,:,:X.shape[2]]



def mirror_fields_vertical(X):
    """
    See mirror_matrices in utility.py
    removes bool mirror_horizontally. Instead there are two separate functions
    """
    # horizontal middle line fixed for square matrices of odd shape
    return np.pad(X,((0,0),(X.shape[1]-1,0),(0,0)),'reflect')[:,:X.shape[1],:]


def transpose_fields(X):
    """
    See transpose_matrices utility.py
    """
    return X.transpose((0,2,1))


# ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']
# Horizontal = ['UP', 'LEFT', 'DOWN', 'RIGHT', 'WAIT', 'BOMB']
mirror_action_horizontal_map = [0, 3, 2, 1, 4, 5]
# Vertical = ['DOWN', 'RIGHT', 'UP', 'LEFT', 'WAIT', 'BOMB']
mirror_action_vertical_map = [2, 1, 0, 3, 4, 5]
# Transpose = ['LEFT', 'DOWN', 'RIGHT', 'UP', 'WAIT', 'BOMB']
transpose_action_map = [3, 2, 1, 0, 4, 5]


def mirror_action_horizontal(a):
    """
    See mirror_actions in utility.py
    """
    return mirror_action_horizontal_map[a]


def mirror_action_vertical(a):
    """
    See mirror_actions in utility.py
    """
    return mirror_action_vertical_map[a]


def transpose_action(a):
    """
    See tranpose_actions in utility.py
    """
    return transpose_action_map[a]


def mirror_cross_horizontal(danger_zone):
    return danger_zone[[0, 4, 5, 6, 1, 2, 3, 7, 8, 9, 10, 11, 12]]

def mirror_cross_vertical(danger_zone):
    return danger_zone[[0, 1, 2, 3, 4, 5, 6, 10, 11, 12, 7, 8, 9]]

def transpose_cross(danger_zone):
    return danger_zone[[0, 10, 11, 12, 7, 8, 9, 4, 5, 6, 1, 2, 3]]
    

if __name__ == "__main__":
    old_game_state_features = np.arange(0, 8).reshape((2, 2, 2))

    # Turn all fields in old_game_state_features
    #old_game_state_features_A = old_game_state_features
    old_game_state_features_B = transpose_fields(old_game_state_features)
    old_game_state_features_C = mirror_fields_horizontal(old_game_state_features)
    old_game_state_features_D = mirror_fields_vertical(old_game_state_features)
    old_game_state_features_E = transpose_fields(old_game_state_features_C)
    old_game_state_features_F = transpose_fields(old_game_state_features_D)
    old_game_state_features_G = mirror_fields_horizontal(old_game_state_features_D)
    old_game_state_features_H = mirror_fields_horizontal(old_game_state_features_E)
    
    assert(np.all(old_game_state_features_B == np.array([[[0, 2], [1, 3]], [[4, 6], [5, 7]]])))
    assert(np.all(old_game_state_features_C == np.array([[[1, 0], [3, 2]], [[5, 4], [7, 6]]])))
    assert(np.all(old_game_state_features_D == np.array([[[2, 3], [0, 1]], [[6, 7], [4, 5]]])))
    assert(np.all(old_game_state_features_E == np.array([[[1, 3], [0, 2]], [[5, 7], [4, 6]]])))
    assert(np.all(old_game_state_features_F == np.array([[[2, 0], [3, 1]], [[6, 4], [7, 5]]])))
    assert(np.all(old_game_state_features_G == np.array([[[3, 2], [1, 0]], [[7, 6], [5, 4]]])))
    assert(np.all(old_game_state_features_H == np.array([[[3, 1], [2, 0]], [[7, 5], [6, 4]]])))
    print("Field Tests passed!")

    # Turn actions
    action_B = []
    action_C = []
    action_D = []
    action_E = []
    action_F = []
    action_G = []
    action_H = []

    for self_action in range(0, 6):
        action_B.append(transpose_action(self_action))
        action_C.append(mirror_action_horizontal(self_action))
        action_D.append(mirror_action_vertical(self_action))
        action_E.append(transpose_action(action_C[self_action]))
        action_F.append(transpose_action(action_D[self_action]))
        action_G.append(mirror_action_horizontal(action_D[self_action]))
        action_H.append(mirror_action_horizontal(action_E[self_action]))

    assert(action_B == [3, 2, 1, 0, 4, 5])
    assert(action_C == [0, 3, 2, 1, 4, 5])
    assert(action_D == [2, 1, 0, 3, 4, 5])
    assert(action_E == [3, 0, 1, 2, 4, 5])
    assert(action_F == [1, 2, 3, 0, 4, 5])
    assert(action_G == [2, 3, 0, 1, 4, 5])
    assert(action_H == [1, 0, 3, 2, 4, 5])


    print("Action Tests passed!")
    print("All Tests passed!")