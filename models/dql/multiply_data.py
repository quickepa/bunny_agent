import numpy as np


def mirror_positions(v, mirror_horizontally=True):
    '''
    This function mirrors positions in a position vector within our game board of fixed size 17x17.
    The mirroring can go either horizontally or vertically.
    '''
    if mirror_horizontally:
        v_new = np.array([16 - v[:,0],v[:,1]]).T
        return np.where(v_new == 16,0,v_new) # Re-map values to 0, because if an entry is 0, it should stay 0 (bec. no coin present here)
    else:
        v_new = np.array([v[:,0],16 - v[:,1]]).T
        return np.where(v_new == 16,0,v_new) # because if entry is 0, it should stay 0 ( e.g. bec. no coin present here)


def transpose_positions(v):
    '''
    This function transposes the positions in a position vector of shape Nx2.
    '''
    return v[:,[1,0]]


def mirror_danger_zone(danger_zone, mirror_horizontally=True):
    '''
    Mirror the danger_zone.
    '''
    if mirror_horizontally:
        return danger_zone[[0, 4, 5, 6, 1, 2, 3, 7, 8, 9, 10, 11, 12]] # 
    else:
        return danger_zone[[0, 1, 2, 3, 4, 5, 6, 10, 11, 12, 7, 8, 9]] #


def transpose_danger_zone(danger_zone):
    '''
    Transpose the danger_zone.
    '''
    #return danger_zone[[0, 10, 11, 12, 7, 8, 9, 4, 5, 6, 1, 2, 3]] # old version
    return danger_zone[[0, 7, 8, 9, 10, 11, 12, 1, 2, 3, 4, 5, 6]]

    
def mirror_environment(X,mirror_horizontally=True):
    '''
    This function mirrors the environment features of a single of states.
    X is a 1x4 vector, containing ('unten','oben','rechts','links')
    '''
    if mirror_horizontally:
        return X[[0,1,3,2]] # exchange values of 'rechts' and 'links'
    else:
        return X[[1,0,2,3]] # exchange values of 'oben and 'unten'


def transpose_environment(X):
    '''
    This function transposes a single environment. 
    X is a 1x4 vector , containing ('unten','oben','rechts','links')
    '''
    return X[[2,3,0,1]] # transpose all values ('oben' <-> 'links' and 'unten' <-> 'rechts')


def mirror_action(a,mirror_horizontally=True):
    '''
    Mirror a single action. (0='UP', 1='RIGHT', 2='DOWN', 3='LEFT')
    '''
    if mirror_horizontally:
        if a == 0:
            return a
        elif a == 1:
            return 3.
        elif a == 2:
            return a
        elif a == 3:
            return 1.
        else:
            return a # WAIT and BOMB are invariant
    else:
        if a == 0:
            return 2.
        elif a == 1:
            return a
        elif a == 2:
            return 0.
        elif a == 3:
            return a
        else:
            return a # WAIT and BOMB are invariant


def transpose_action(a):
    '''
    Transpose a single action
    '''
    if a == 0:
        return 3.
    elif a == 1:
        return 2.
    elif a == 2:
        return 1.
    elif a == 3:
        return 0.
    else:
        return a # WAIT and BOMB remain invariant
        

def multiply_state_features_old(game_state_features):

    # Deal with all positions (self_pos, closest_coin_pos, crate_pos_closest)
    positions_A = game_state_features[:8].reshape(4,2)
    positions_H = transpose_positions(positions_A)
    positions_E = mirror_positions(positions_A)
    positions_G = mirror_positions(positions_A,mirror_horizontally=False)
    positions_B = transpose_positions(positions_E)
    positions_D = transpose_positions(positions_G)
    positions_C = mirror_positions(positions_G)
    positions_F = mirror_positions(positions_B)

    # Deal with the environment
    environment_A = game_state_features[8:12]
    environment_H = transpose_environment(environment_A)
    environment_E = mirror_environment(environment_A)
    environment_G = mirror_environment(environment_A,mirror_horizontally=False)
    environment_B = transpose_environment(environment_E)
    environment_D = transpose_environment(environment_G)
    environment_C = mirror_environment(environment_G)
    environment_F = mirror_environment(environment_B)

    # Deal with danger zone:
    danger_zone_A = game_state_features[12:25]
    danger_zone_H = transpose_danger_zone(danger_zone_A)
    danger_zone_E = mirror_danger_zone(danger_zone_A)
    danger_zone_G = mirror_danger_zone(danger_zone_A,mirror_horizontally=False)
    danger_zone_B = transpose_danger_zone(danger_zone_E)
    danger_zone_D = transpose_danger_zone(danger_zone_G)
    danger_zone_C = mirror_danger_zone(danger_zone_G)
    danger_zone_F = mirror_danger_zone(danger_zone_B)

    # The bomb_availability is invariant
    bomb_available = game_state_features[-1]

    # Construct multiplicates of game_state_features
    features_A = game_state_features
    features_B = np.concatenate([positions_B.flatten(),environment_B,danger_zone_B,[bomb_available]])
    features_C = np.concatenate([positions_C.flatten(),environment_C,danger_zone_C,[bomb_available]])
    features_D = np.concatenate([positions_D.flatten(),environment_D,danger_zone_D,[bomb_available]])
    features_E = np.concatenate([positions_E.flatten(),environment_E,danger_zone_E,[bomb_available]])
    features_F = np.concatenate([positions_F.flatten(),environment_F,danger_zone_F,[bomb_available]])
    features_G = np.concatenate([positions_G.flatten(),environment_G,danger_zone_G,[bomb_available]])
    features_H = np.concatenate([positions_H.flatten(),environment_H,danger_zone_H,[bomb_available]])

    return (features_B, features_C, features_D, features_E, features_F, features_G, features_H)

# def append_transition_8times(old_game_state_features, self_action, new_game_state_features, reward, terminal):
    
#     #
#     # Start with old_game_state_features
#     #

#     # Deal with all positions (self_pos, closest_coin_pos, crate_pos_closest)
#     positions_old_A = old_game_state_features[:8].reshape(4,2)
#     positions_old_H = transpose_positions(positions_old_A)
#     positions_old_E = mirror_positions(positions_old_A)
#     positions_old_G = mirror_positions(positions_old_A,mirror_horizontally=False)
#     positions_old_B = transpose_positions(positions_old_E)
#     positions_old_D = transpose_positions(positions_old_G)
#     positions_old_C = mirror_positions(positions_old_G)
#     positions_old_F = mirror_positions(positions_old_B)

#     # Deal with the environment
#     environment_old_A = old_game_state_features[8:12]
#     environment_old_H = transpose_environment(environment_old_A)
#     environment_old_E = mirror_environment(environment_old_A)
#     environment_old_G = mirror_environment(environment_old_A,mirror_horizontally=False)
#     environment_old_B = transpose_environment(environment_old_E)
#     environment_old_D = transpose_environment(environment_old_G)
#     environment_old_C = mirror_environment(environment_old_G)
#     environment_old_F = mirror_environment(environment_old_B)

#     # Deal with danger zone:
#     danger_zone_old_A = old_game_state_features[13:26]
#     danger_zone_old_H = transpose_danger_zone(danger_zone_old_A)
#     danger_zone_old_E = mirror_danger_zone(danger_zone_old_A)
#     danger_zone_old_G = mirror_danger_zone(danger_zone_old_A,mirror_horizontally=False)
#     danger_zone_old_B = transpose_danger_zone(danger_zone_old_E)
#     danger_zone_old_D = transpose_danger_zone(danger_zone_old_G)
#     danger_zone_old_C = mirror_danger_zone(danger_zone_old_G)
#     danger_zone_old_F = mirror_danger_zone(danger_zone_old_B)

#     # The bomb_availability is invariant
#     bomb_available_old = old_game_state_features[-1]

#     # Construct multiplicates of old_game_state_features
#     old_features_A = old_game_state_features
#     old_features_B = np.concatenate([positions_old_B.flatten(),environment_old_B,danger_zone_old_B,[bomb_available_old]])
#     old_features_C = np.concatenate([positions_old_C.flatten(),environment_old_C,danger_zone_old_C,[bomb_available_old]])
#     old_features_D = np.concatenate([positions_old_D.flatten(),environment_old_D,danger_zone_old_D,[bomb_available_old]])
#     old_features_E = np.concatenate([positions_old_E.flatten(),environment_old_E,danger_zone_old_E,[bomb_available_old]])
#     old_features_F = np.concatenate([positions_old_F.flatten(),environment_old_F,danger_zone_old_F,[bomb_available_old]])
#     old_features_G = np.concatenate([positions_old_G.flatten(),environment_old_G,danger_zone_old_G,[bomb_available_old]])
#     old_features_H = np.concatenate([positions_old_H.flatten(),environment_old_H,danger_zone_old_H,[bomb_available_old]])

#     #
#     # Deal with the action
#     #
#     action_A = self_action
#     action_H = transpose_action(action_A)
#     action_E = mirror_action(action_A)
#     action_G = mirror_action(action_A,mirror_horizontally=False)
#     action_B = transpose_action(action_E)
#     action_D = transpose_action(action_G)
#     action_C = mirror_action(action_G)
#     action_F = mirror_action(action_B)

#     #
#     # Rewards and terminal remain invariant , so just use them for each multiplication later
#     #
    
#     #
#     # Do same with new_game_state_features as with old ones
#     #

#     # Deal with all positions (self_pos, closest_coin_pos, crate_pos_closest)
#     positions_new_A = new_game_state_features[:8].reshape(4,2)
#     positions_new_H = transpose_positions(positions_new_A)
#     positions_new_E = mirror_positions(positions_new_A)
#     positions_new_G = mirror_positions(positions_new_A,mirror_horizontally=False)
#     positions_new_B = transpose_positions(positions_new_E)
#     positions_new_D = transpose_positions(positions_new_G)
#     positions_new_C = mirror_positions(positions_new_G)
#     positions_new_F = mirror_positions(positions_new_B)

#     # Deal with the environment
#     environment_new_A = new_game_state_features[8:12]
#     environment_new_H = transpose_environment(environment_new_A)
#     environment_new_E = mirror_environment(environment_new_A)
#     environment_new_G = mirror_environment(environment_new_A,mirror_horizontally=False)
#     environment_new_B = transpose_environment(environment_new_E)
#     environment_new_D = transpose_environment(environment_new_G)
#     environment_new_C = mirror_environment(environment_new_G)
#     environment_new_F = mirror_environment(environment_new_B)

#     # Deal with danger zone:
#     danger_zone_new_A = new_game_state_features[13:26]
#     danger_zone_new_H = transpose_danger_zone(danger_zone_new_A)
#     danger_zone_new_E = mirror_danger_zone(danger_zone_new_A)
#     danger_zone_new_G = mirror_danger_zone(danger_zone_new_A,mirror_horizontally=False)
#     danger_zone_new_B = transpose_danger_zone(danger_zone_new_E)
#     danger_zone_new_D = transpose_danger_zone(danger_zone_new_G)
#     danger_zone_new_C = mirror_danger_zone(danger_zone_new_G)
#     danger_zone_new_F = mirror_danger_zone(danger_zone_new_B)

#     # The bomb_availability is invariant
#     bomb_available_new = new_game_state_features[-1]

#     # Construct multiplicates of new_game_state_features
#     new_features_A = new_game_state_features
#     new_features_B = np.concatenate([positions_new_B.flatten(),environment_new_B,danger_zone_new_B,[bomb_available_new]])
#     new_features_C = np.concatenate([positions_new_C.flatten(),environment_new_C,danger_zone_new_C,[bomb_available_new]])
#     new_features_D = np.concatenate([positions_new_D.flatten(),environment_new_D,danger_zone_new_D,[bomb_available_new]])
#     new_features_E = np.concatenate([positions_new_E.flatten(),environment_new_E,danger_zone_new_E,[bomb_available_new]])
#     new_features_F = np.concatenate([positions_new_F.flatten(),environment_new_F,danger_zone_new_F,[bomb_available_new]])
#     new_features_G = np.concatenate([positions_new_G.flatten(),environment_new_G,danger_zone_new_G,[bomb_available_new]])
#     new_features_H = np.concatenate([positions_new_H.flatten(),environment_new_H,danger_zone_new_H,[bomb_available_new]])

#     A = (old_features_A, self_action, new_features_A, reward, terminal)
#     B = (old_features_B, action_B, new_features_B, reward, terminal)
#     C = (old_features_C, action_C, new_features_C, reward, terminal)
#     D = (old_features_D, action_D, new_features_D, reward, terminal)
#     E = (old_features_E, action_E, new_features_E, reward, terminal)
#     F = (old_features_F, action_F, new_features_F, reward, terminal)
#     G = (old_features_G, action_G, new_features_G, reward, terminal)
#     H = (old_features_H, action_H, new_features_H, reward, terminal)
#     return A,B,C,D,E,F,G,H


def multiply_state_features(game_state_features):
    # Deal with the coin_features
    coin_A = game_state_features[:4]
    #print('coin_A',coin_A)
    coin_H = transpose_environment(coin_A)
    coin_E = mirror_environment(coin_A)
    coin_G = mirror_environment(coin_A,mirror_horizontally=False)
    coin_B = transpose_environment(coin_E)
    coin_D = transpose_environment(coin_G)
    coin_C = mirror_environment(coin_G)
    coin_F = mirror_environment(coin_B)

    # Deal with the crate_features
    crate_A = game_state_features[4:8]
    #print('crate_A:',crate_A)
    crate_H = transpose_environment(crate_A)
    crate_E = mirror_environment(crate_A)
    crate_G = mirror_environment(crate_A,mirror_horizontally=False)
    crate_B = transpose_environment(crate_E)
    crate_D = transpose_environment(crate_G)
    crate_C = mirror_environment(crate_G)
    crate_F = mirror_environment(crate_B)
    crate_distance = game_state_features[8] # invariant under rotation/tranposition/mirroring

    # Deal with the safe_features
    safe_A = game_state_features[9:13]
    #print('safe_A',safe_A)
    safe_H = transpose_environment(safe_A)
    safe_E = mirror_environment(safe_A)
    safe_G = mirror_environment(safe_A,mirror_horizontally=False)
    safe_B = transpose_environment(safe_E)
    safe_D = transpose_environment(safe_G)
    safe_C = mirror_environment(safe_G)
    safe_F = mirror_environment(safe_B)
    safe_distance = game_state_features[13] # invariant under rotation/tranposition/mirroring

    # Deal with the other_features (positions of other agents)
    enemy_A = game_state_features[14:18]
    #print("enemy_A",enemy_A)
    enemy_H = transpose_environment(enemy_A)
    enemy_E = mirror_environment(enemy_A)
    enemy_G = mirror_environment(enemy_A,mirror_horizontally=False)
    enemy_B = transpose_environment(enemy_E)
    enemy_D = transpose_environment(enemy_G)
    enemy_C = mirror_environment(enemy_G)
    enemy_F = mirror_environment(enemy_B)
    enemy_distance = game_state_features[18]

    bomb_available = game_state_features[19] # invariant under rotation/tranposition/mirroring
    safe_available = game_state_features[20] # invariant under rotation/tranposition/mirroring
    # Construct multiplicates of game_state_features
    #features_A = game_state_features
    features_B = np.concatenate([coin_B,crate_B,[crate_distance],safe_B,[safe_distance],enemy_B,[enemy_distance],[bomb_available],[safe_available]])
    features_C = np.concatenate([coin_C,crate_C,[crate_distance],safe_C,[safe_distance],enemy_C,[enemy_distance],[bomb_available],[safe_available]])
    features_D = np.concatenate([coin_D,crate_D,[crate_distance],safe_D,[safe_distance],enemy_D,[enemy_distance],[bomb_available],[safe_available]])
    features_E = np.concatenate([coin_E,crate_E,[crate_distance],safe_E,[safe_distance],enemy_E,[enemy_distance],[bomb_available],[safe_available]])
    features_F = np.concatenate([coin_F,crate_F,[crate_distance],safe_F,[safe_distance],enemy_F,[enemy_distance],[bomb_available],[safe_available]])
    features_G = np.concatenate([coin_G,crate_G,[crate_distance],safe_G,[safe_distance],enemy_G,[enemy_distance],[bomb_available],[safe_available]])
    features_H = np.concatenate([coin_H,crate_H,[crate_distance],safe_H,[safe_distance],enemy_H,[enemy_distance],[bomb_available],[safe_available]])

    return (features_B, features_C, features_D, features_E, features_F, features_G, features_H)