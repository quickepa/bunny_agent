import torch as T
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
from random import shuffle
import pickle

import events as e

from ..IBrain import IBrain
from ..utility import state_to_features, reward_from_events, ACTIONS
from ..policy import softmax_policy, eps_greedy_policy
from .multiply import mirror_fields_horizontal, mirror_fields_vertical, transpose_fields, \
                                mirror_action_horizontal, mirror_action_vertical, transpose_action
from .multiply_data import multiply_state_features

DIDNT_DODGE = "DIDNT_DODGE"
HOPELESS_BOMB = "HOPELESS_BOMB"

class DeepQNetwork(nn.Module):
    def __init__(self, lr, input_dims, fc1_dims, fc2_dims, n_actions):
        super(DeepQNetwork, self).__init__()
        self.input_dims = input_dims
        self.fc1_dims = fc1_dims
        self.fc2_dims = fc2_dims
        self.n_actions = n_actions
        self.fc1 = nn.Linear(*self.input_dims, self.fc1_dims)
        self.fc2 = nn.Linear(self.fc1_dims, self.fc2_dims)
        self.fc3 = nn.Linear(self.fc2_dims, self.n_actions)
        self.optimizer = optim.Adam(self.parameters(), lr=lr)
        self.loss = nn.MSELoss()
        self.device = T.device('cuda:0' if T.cuda.is_available() else 'cpu')
        print("CUDA AVAILABLE: ", T.cuda.is_available())
        self.to(self.device)

    def forward(self, state):
        x = F.relu(self.fc1(state))
        x = F.relu(self.fc2(x))
        actions = self.fc3(x)     
        return actions


class DqlBrain(IBrain):
    """
    """
    def __init__(self):
         # Network parameters
        self.input_dims = [21]#[20]#[26]#[196]#[9]#[1156]#[867]
        self.n_actions = len(ACTIONS)
        self.action_space = np.arange(0, 6)

        # Memory
        self.max_mem_size = 100000
        self.batch_size = 64*8
        self.mem_cntr = 0
        self.state_memory = np.zeros((self.max_mem_size, *self.input_dims), dtype=np.float32)
        self.new_state_memory = np.zeros((self.max_mem_size, *self.input_dims), dtype=np.float32)
        self.action_memory = np.zeros((self.max_mem_size), dtype=np.int32)
        self.reward_memory = np.zeros((self.max_mem_size), dtype=np.float32)
        self.terminal_memory = np.zeros((self.max_mem_size), dtype=np.bool)
        
        # Learning parameters
        self.gamma = 0.95
        self.lr = 1e-4#0.003

        # The network
        self.network = DeepQNetwork(lr=self.lr, input_dims=self.input_dims,
                                    fc1_dims=256, fc2_dims=256, n_actions=self.n_actions)
        # Policy parameters
        self.epsilon = 0.5 # 1.0
        self.eps_min = 0.05
        self.eps_games = 120000 #70000
        self.eps_dec = (self.epsilon-self.eps_min)/self.eps_games # 2e-6 

        self.rho = 1.0

        # Analytics
        self.current_score = {} #0
        self.actual_current_score = {}
        self.scores = {} #np.empty((0, 2))
        self.survived_steps = {}
        self.score_file = './analytics/dql/scores' # .npy is added while saving

        self.loss = np.empty((0))
        self.loss_file = './analytics/dql/loss.npy'

    def prepare_training(self, name):
        # Prepare analytics
        if name not in self.current_score:
            self.current_score[name] = 0
        if name not in self.actual_current_score:
            self.actual_current_score[name] = 0
        if name not in self.scores:
            self.scores[name] = np.empty((0, 4))
        if name not in self.survived_steps:
            self.survived_steps[name] = 0


    
    def save(self, file_path, agent_name):
        # Deactivate CUDA, so we can use models trained on GPU on other computers
        #self.network.device = T.device('cpu')
        #self.network.to(self.network.device)
        
        # Pickle the model
        with open(file_path, "wb") as file:
            #pickle.dump(self, file)
            T.save(self, file)
        # Save analytics
        np.save(self.score_file + "_" + agent_name + ".npy", self.scores[agent_name])
        np.save(self.loss_file, self.loss)

        # Activate CUDA if available
        #self.network.device = T.device('cuda:0' if T.cuda.is_available() else 'cpu')
        #self.network.to(self.network.device)


    def append_transition(self, old_game_state, self_action, new_game_state, events, agent_name):
        
        # Ignore State before game started
        if self_action == None:
            return 
        
        # Compute features from state dicts
        old_game_state_features = self.state_to_features(old_game_state)
        new_game_state_features = np.zeros(old_game_state_features.shape) \
                                 if new_game_state == None else self.state_to_features(new_game_state)
        action = ACTIONS.index(self_action)
        terminal = (new_game_state == None)

        # # Custom events
        # # Penalty for hopeless bomb: if safe_available false and still BOMB chosen
        # if old_game_state_features[-1] == 0.0 and action == 5:
        #     events.append(HOPELESS_BOMB)

        # # Penalty if dodge direction is ignored.
        # moves_one_hot = np.array([[0, 0, 0, 1], [1, 0, 0, 0], [0, 0, 1, 0], [0, 1, 0, 0], [4, 4, 4, 4], [5, 5, 5, 5]], dtype=np.float32)
        # #print(action, self_action, moves_one_hot[action])
        # safe_feature = old_game_state_features[9:13] 
        # # if safe_feature shows direction and the action chosen is not that direcion
        # if np.any(safe_feature != np.array([0.0, 0.0, 0.0, 0.0])) and np.any(safe_feature != moves_one_hot[action]):
        #     #print(safe_feature,  moves_one_hot[action])
        #     events.append(DIDNT_DODGE)
        
        # Compute reward and store it for analytics
        reward = self.reward_from_events(events)
        actual_reward = self.actual_reward_from_events(events)
        self.current_score[agent_name] += reward
        self.actual_current_score[agent_name] += actual_reward

        # Write entry into memory
        self.append_transition_times_1(old_game_state_features, action, new_game_state_features, reward, terminal)
        #self.append_transition_times_8(old_game_state_features, action, new_game_state_features, reward, terminal)
        #self.append_transition_times_8_2(old_game_state_features, action, new_game_state_features, reward, terminal)
        return


    def append_transition_times_1(self, old_game_state_features, self_action, new_game_state_features, reward, terminal):
        # Ringbuffer
        index = self.mem_cntr % self.max_mem_size
        # Add transition to buffer
        self.state_memory[index] = old_game_state_features.flatten()
        self.new_state_memory[index] = new_game_state_features.flatten()
        self.action_memory[index] = self_action
        self.reward_memory[index] = reward
        self.terminal_memory[index] = terminal
        
        self.mem_cntr += 1
        

    def append_transition_times_8(self, old_game_state_features, self_action, new_game_state_features, reward, terminal):
        # Multiply old state
        old_game_state_features_ = multiply_state_features(old_game_state_features)

        # Multiply new state
        new_game_state_features_ = multiply_state_features(new_game_state_features)
        
        # Multiply actions
        action_A = self_action
        action_H = transpose_action(action_A)
        action_E = mirror_action_horizontal(action_A)
        action_G = mirror_action_vertical(action_A)
        action_B = transpose_action(action_E)
        action_D = transpose_action(action_G)
        action_C = mirror_action_horizontal(action_G)
        action_F = mirror_action_horizontal(action_B)

        # Add all 8 transitions to memory
        self.append_transition_times_1(old_game_state_features, self_action, new_game_state_features, reward, terminal)
        self.append_transition_times_1(old_game_state_features_[0], action_B, new_game_state_features_[0], reward, terminal)
        self.append_transition_times_1(old_game_state_features_[1], action_C, new_game_state_features_[1], reward, terminal)
        self.append_transition_times_1(old_game_state_features_[2], action_D, new_game_state_features_[2], reward, terminal)
        self.append_transition_times_1(old_game_state_features_[3], action_E, new_game_state_features_[3], reward, terminal)
        self.append_transition_times_1(old_game_state_features_[4], action_F, new_game_state_features_[4], reward, terminal)
        self.append_transition_times_1(old_game_state_features_[5], action_G, new_game_state_features_[5], reward, terminal)
        self.append_transition_times_1(old_game_state_features_[6], action_H, new_game_state_features_[6], reward, terminal)
        return 

    def append_transition_times_8_2(self, old_game_state_features, self_action, new_game_state_features, reward, terminal):
        # Turn all fields in old_game_state_features
        #old_game_state_features_A = old_game_state_features
        old_game_state_features_B = transpose_fields(old_game_state_features)
        old_game_state_features_C = mirror_fields_horizontal(old_game_state_features)
        old_game_state_features_D = mirror_fields_vertical(old_game_state_features)
        old_game_state_features_E = transpose_fields(old_game_state_features_C)
        old_game_state_features_F = transpose_fields(old_game_state_features_D)
        old_game_state_features_G = mirror_fields_horizontal(old_game_state_features_D)
        old_game_state_features_H = mirror_fields_horizontal(old_game_state_features_E)
        
        # Turn all fields in new_game_state_features
        #new_game_state_features_A = new_game_state_features
        new_game_state_features_B = transpose_fields(new_game_state_features)
        new_game_state_features_C = mirror_fields_horizontal(new_game_state_features)
        new_game_state_features_D = mirror_fields_vertical(new_game_state_features)
        new_game_state_features_E = transpose_fields(new_game_state_features_C)
        new_game_state_features_F = transpose_fields(new_game_state_features_D)
        new_game_state_features_G = mirror_fields_horizontal(new_game_state_features_D)
        new_game_state_features_H = mirror_fields_horizontal(new_game_state_features_E)
        
        # Turn actions
        #action_A = self_action
        action_B = transpose_action(self_action)
        action_C = mirror_action_horizontal(self_action)
        action_D = mirror_action_vertical(self_action)
        action_E = transpose_action(action_C)
        action_F = transpose_action(action_D)
        action_G = mirror_action_horizontal(action_D)
        action_H =  mirror_action_horizontal(action_E)

        # reward stays the same as long as its symetrical!!!
        
        # Add all 8 transitions to memory
        self.append_transition_times_1(old_game_state_features, self_action, new_game_state_features, reward, terminal)
        self.append_transition_times_1(old_game_state_features_B, action_B, new_game_state_features_B, reward, terminal)
        self.append_transition_times_1(old_game_state_features_C, action_C, new_game_state_features_C, reward, terminal)
        self.append_transition_times_1(old_game_state_features_D, action_D, new_game_state_features_D, reward, terminal)
        self.append_transition_times_1(old_game_state_features_E, action_E, new_game_state_features_E, reward, terminal)
        self.append_transition_times_1(old_game_state_features_F, action_F, new_game_state_features_F, reward, terminal)
        self.append_transition_times_1(old_game_state_features_G, action_G, new_game_state_features_G, reward, terminal)
        self.append_transition_times_1(old_game_state_features_H, action_H, new_game_state_features_H, reward, terminal)
        return
    

    def policy(self, state):
        #Q_actions = self.estimate_all_actions(self.state_to_features(state).flatten()
        #return eps_greedy_policy(Q_actions, self.eps)
        #return softmax_policy(Q_actions, self.rho)
        return self.eps_greedy_policy(self.state_to_features(state).flatten())

    '''
    def estimate_all_actions(self, observation):
        state = T.tensor([observation]).to(self.network.device)
        Q_actions = self.network.forward(state)
        action = T.argmax(Q_actions).item()
        return action
    '''

    def eps_greedy_policy(self, observation):
        #self.epsilon = 0.01
        #self.network.device = T.device('cuda:0' if T.cuda.is_available() else 'cpu')
        if np.random.random() > self.epsilon:
            state = T.tensor([observation]).to(self.network.device)
            Q_actions = self.network.forward(state)
            #print("Q_actions: ", Q_actions)
            action = T.argmax(Q_actions).item()
        else:
            action = np.random.choice(self.action_space)
        return ACTIONS[action]


    def train_per_move(self, agent_name):
        if self.mem_cntr < self.batch_size:
            return
        
        self.network.optimizer.zero_grad()
        
        # Sample memory
        max_mem = min(self.mem_cntr, self.max_mem_size)
        batch = np.random.choice(max_mem, self.batch_size, replace=False)

        batch_index = np.arange(self.batch_size, dtype=np.int32)

        state_batch = T.tensor(self.state_memory[batch]).to(self.network.device)
        new_state_batch = T.tensor(self.new_state_memory[batch]).to(self.network.device)
        reward_batch = T.tensor(self.reward_memory[batch]).to(self.network.device)
        terminal_batch = T.tensor(self.terminal_memory[batch]).to(self.network.device)

        action_batch = self.action_memory[batch]

        q_eval = self.network.forward(state_batch)[batch_index, action_batch]
        q_next = self.network.forward(new_state_batch)
        q_next[terminal_batch] = 0.0

        q_target = reward_batch + self.gamma * T.max(q_next, dim=1)[0]

        loss = self.network.loss(q_target, q_eval).to(self.network.device)
        loss.backward()
        self.network.optimizer.step()

        #self.epsilon = self.epsilon - self.eps_dec if self.epsilon > self.eps_min else self.eps_min

        self.loss = np.append(self.loss, loss.item())
        self.survived_steps[agent_name] += 1


    def train(self, agent_name):
        self.train_per_move(agent_name)

        self.epsilon = self.epsilon - self.eps_dec if self.epsilon > self.eps_min else self.eps_min

        # Reset the score at the end of the epsisode
        self.scores[agent_name] = np.append(self.scores[agent_name], \
                        np.array([[self.current_score[agent_name], 
                                    self.epsilon, self.survived_steps[agent_name], 
                                    self.actual_current_score[agent_name]]]), axis = 0)
        self.current_score[agent_name] = 0
        self.actual_current_score[agent_name] = 0
        self.survived_steps[agent_name] = 0
        #np.save(self.score_file, self.scores)
        #np.save(self.loss_file, self.loss)


    def state_to_features(self, game_state: dict):
        """
        Translate a game state into features.
        Currently a Wrapper for the utility function 'state_to_features()'
        in case we want to try different features for different models.

        Parameters
        ----------
        game_state : dict
            game state to translate

        Returns
        -------
        np.array
            The feature vector.
        """
        return self.state_to_features3(game_state)

        #return self.state_to_features2(game_state)

        # This is the dict before the game begins and after it ends
        if game_state is None:
            return None

        # Walls and Crates
        field = np.array(game_state['field'])

        # Add agents position, so he can figure out, where he stands relative to other stuff
        self_pos = np.array(game_state['self'][3]) # 2

        # Add just possible stepping fields.
        oben = np.array([ game_state['field'][self_pos[0], self_pos[1]+1] ])
        unten = np.array([ game_state['field'][self_pos[0], self_pos[1]-1] ])
        rechts = np.array([ game_state['field'][self_pos[0]+1, self_pos[1]] ])
        links = np.array([ game_state['field'][self_pos[0]-1, self_pos[1]] ])
        umgebung = np.concatenate([oben, unten, rechts, links])

        # Add coin positions, so agent can find them
        coin_pos = np.array(game_state['coins']).flatten()
        coin_pos = np.pad(coin_pos, (0, 18-coin_pos.shape[0]), 'constant')
        coin_pos = np.reshape(coin_pos, (9,2))

        # Get position and distance of closest coin 
        #coin_distance = np.linalg.norm(coin_pos_np-self_pos, axis=1)
        coin_distances = np.sum(np.abs(coin_pos-self_pos), axis=1)
        coin_distances[coin_pos[:, 0] == 0] = 100
        closest_coin_pos = coin_pos[np.argmin(coin_distances), :]
        closest_coin_dist = np.min(coin_distances)

        # Add Bomb positions and timers, so he can figure out to avoid them
        bomb_pos = np.array([[a, b] for (a, b), c in game_state['bombs']])
        bomb_timer = np.array([c for (a, b), c in game_state['bombs']])

        bomb_field = np.zeros(field.shape)
        for j, bomb in enumerate(bomb_pos):
            # risk highest (=4), when bombtimer lowest (=1)
            if bomb_timer[j] == 0:
                continue
            danger = 5 - bomb_timer[j]
            bomb_field[bomb[0], bomb[1]] = danger
            for i in range(1, 4):
                if field[bomb[0]+i, bomb[1]] != -1:
                    bomb_field[bomb[0]+i, bomb[1]] = danger
                else: break
            for i in range(1, 4):
                if field[bomb[0]-i, bomb[1]] != -1:
                    bomb_field[bomb[0]-i, bomb[1]] = danger
                else: break
            for i in range(1, 4):
                if field[bomb[0], bomb[1]+i] != -1:
                    bomb_field[bomb[0], bomb[1]+i] = danger
                else: break
            for i in range(1, 4):
                if field[bomb[0], bomb[1]-i] != -1:
                    bomb_field[bomb[0], bomb[1]-i] = danger
                else: break

        danger_zone = np.zeros((13))
        x, y = self_pos
        danger_zone[0] = bomb_field[x, y]
        for i in range(1, 4):
            if field[self_pos[0]+i, self_pos[1]] != -1:
                danger_zone[0+i] = bomb_field[self_pos[0]+i, self_pos[1]]
            else: break
        for i in range(1, 4):
            if field[self_pos[0]-i, self_pos[1]] != -1:
                danger_zone[3+i] = bomb_field[self_pos[0]-i, self_pos[1]]
            else: break
        for i in range(1, 4):
            if field[self_pos[0], self_pos[1]+i] != -1:
                danger_zone[6+i] = bomb_field[self_pos[0], self_pos[1]+i]
            else: break
        for i in range(1, 4):
            if field[self_pos[0], self_pos[1]-i] != -1:
                danger_zone[9+i] = bomb_field[self_pos[0], self_pos[1]-i]
            else: break

        # Extract create positions as Nx2 array
        crate_pos = np.hstack((np.where(game_state['field']==1)[0].reshape(-1,1),np.where(game_state['field']==1)[1].reshape(-1,1)))
        crate_distances = np.sum(np.abs(crate_pos - self_pos),axis=1)
        #crate_pos_closest = crate_pos[np.argpartition(crate_distances,5)][:5]
        #crate_pos_closest = crate_pos[np.argmin(crate_distances)]
        crate_pos_closest = crate_pos[np.argpartition(crate_distances,2)][:2].flatten()
        crate_pos_closest = np.pad(crate_pos_closest, (0, 4-crate_pos_closest.shape[0]), 'constant')
        '''
        # For normalization we need the mean position of things
        mean_pos = np.array([16.0, 16.0])/2.0
        self_pos = self_pos - mean_pos
        umgebung = umgebung + 0.5
        coin_pos = (coin_pos - mean_pos).flatten()
        coin_distances = coin_distances - (29 + 1)/ 2
        closest_coin_pos = closest_coin_pos - mean_pos
        closest_coin_dist = closest_coin_dist - (29 + 1)/ 2
        #bomb_pos = (bomb_pos - mean_pos).flatten()
        omb_timers = (bomb_timers - 2).flatten()
        crate_pos_closest = (crate_pos_closest - mean_pos).flatten()
        '''
        bomb_available = game_state['self'][2]

        X = np.concatenate([
                            self_pos, 
                            closest_coin_pos, 
                            crate_pos_closest,
                            #[closest_coin_dist],
                            umgebung,
                            danger_zone,
                            [bomb_available]
                        ])

        return X.astype(np.float32)

    def state_to_features2(self,  game_state: dict):
        # This is the dict before the game begins and after it ends
        if game_state is None:
            return None

        # Walls and Crates
        field = np.array(game_state['field'])
        # Coin positions
        coin_pos = np.array(game_state['coins'])
        coin_field = np.zeros(field.shape)
        for coin in coin_pos:
            coin_field[coin[0], coin[1]] = 1

        # Add agents position, so he can figure out, where he stands relative to other stuff
        self_pos = np.array(game_state['self'][3]) # 2
        agent_field = np.zeros(field.shape)
        agent_field[self_pos[0], self_pos[1]] = 1

        # Add enemy agent positions
        enemies = game_state['others']
        for enemy in enemies:
            enemy_pos = enemy[3]
            agent_field[enemy_pos[0], enemy_pos[1]] = -1

        # Add bomb positions
        bomb_pos = np.array([[a, b] for (a, b), c in game_state['bombs']])
        bomb_timer = np.array([c for (a, b), c in game_state['bombs']])

        bomb_field = np.zeros(field.shape)
        for j, bomb in enumerate(bomb_pos):
            # risk highest (=4), when bombtimer lowest (=1)
            if bomb_timer[j] == 0:
                continue
            danger = (5 - bomb_timer[j])# / 3
            bomb_field[bomb[0], bomb[1]] = danger
            for i in range(1, 4):
                if field[bomb[0]+i, bomb[1]] != -1:
                    bomb_field[bomb[0]+i, bomb[1]] = danger
                else: break
            for i in range(1, 4):
                if field[bomb[0]-i, bomb[1]] != -1:
                    bomb_field[bomb[0]-i, bomb[1]] = danger
                else: break
            for i in range(1, 4):
                if field[bomb[0], bomb[1]+i] != -1:
                    bomb_field[bomb[0], bomb[1]+i] = danger
                else: break
            for i in range(1, 4):
                if field[bomb[0], bomb[1]-i] != -1:
                    bomb_field[bomb[0], bomb[1]-i] = danger
                else: break
        #print(bomb_field.T)

        # Get subset around player
        radius = 3 # =7x7
        pad = radius - 1
        agent_field_pad = np.pad(agent_field, ((pad, pad), (pad, pad)), 'constant', constant_values=(0, 0))
        coin_field_pad = np.pad(coin_field, ((pad, pad), (pad, pad)), 'constant', constant_values=(0, 0))
        field_pad =  np.pad(field, ((pad, pad), (pad, pad)), 'constant', constant_values=(-1, -1))
        bomb_field_pad = np.pad(bomb_field, ((pad, pad), (pad, pad)), 'constant', constant_values=(0, 0))

        x, y = self_pos + [pad, pad]

        cut = np.s_[(x-radius) : (x+radius+1), (y-radius) : (y+radius+1)]
        
        X = np.stack([agent_field_pad[cut],
                            coin_field_pad[cut],
                            field_pad[cut],
                            bomb_field_pad[cut]
                            ])

        # print("AGENT FIELD: ")
        # print(bomb_field)
        # print("AGENT FIELD CUT: ")
        # print(X[3])
        return X.astype(np.float32)

    def state_to_features3(self, game_state: dict):
        """
        Translate a game state into features.
        Currently a Wrapper for the utility function 'state_to_features()'
        in case we want to try different features for different models.

        Parameters
        ----------
        game_state : dict
            game state to translate

        Returns
        -------
        np.array
            The feature vector.
        """
        # This is the dict before the game begins and after it ends
        if game_state is None:
            return None

        # Extract Walls and Crates
        field = game_state['field']
        # Agent position
        self_pos = game_state['self'][3] # self position as tuple (x,y)
        # Bomb position and timers
        bomb_pos = np.array([[a, b] for (a, b), c in game_state['bombs']])
        bomb_timer = np.array([c for (a, b), c in game_state['bombs']])
        # Enemy positions
        other_targets = [other[3] for other in game_state['others']]
        # Extract coin targets
        coin_targets = game_state['coins'] # list of tuples containing visible coin positions or empty list
        # Extract crate targets
        crate_pos = np.hstack((np.where(field==1)[0].reshape(-1,1),np.where(field==1)[1].reshape(-1,1)))
        crate_targets = [*crate_pos] # list of 1D vectors (stored as np.arrays) containing (x,y) coordinates of all crates

        # Walkable area for look_for_targets
        # There are three of these
        # Targets always have to be walkable, so every call of look_for_targets
        # needs separate free_space!!
        free_space = (field == 0) # 1. coins and safespace search
        free_space_crates = (field != -1) # 2. crate search, crates walkable
        free_space_others = (field == 0) # 3. enemie search
        
        # Bombs are not walkable...
        for b in bomb_pos:
            free_space[b[0], b[1]] = False
            free_space_crates[b[0], b[1]] = False
            free_space_others[b[0], b[1]] = False

        # ...except for bombs just placed
        free_space[self_pos[0], self_pos[1]] = True
        free_space_crates[self_pos[0], self_pos[1]] = True
        free_space_others[self_pos[0], self_pos[1]] = True

       
        # Enemies are not walkable except for free_space_others
        for o in other_targets:
            free_space_crates[o[0], o[1]] = False 
            free_space_others[o[0], o[1]] = True
            free_space[o[0], o[1]] = False
        
        #print("FREE SPACE:")
        #print(free_space.astype(np.int32).T)
        #print("FREE SPACE CRATES:")
        #print(free_space_crates.astype(np.int32).T)
        #print("FREE SPACE OTHERS:")
        #print(free_space_others.astype(np.int32).T)
        #print("FIELD:")
        #print(field.T)
        #x, y = self_pos[0], self_pos[1]
        #free = [free_space[x+1 , y], free_space[x-1 , y], free_space[x, y+1], free_space[x , y-1],]
        #print('self_pos',self_pos, 'bomb_pos', bomb_pos, 'free', free)
        #print('crate_targets:',crate_targets)
        #print('coin_targets:',coin_targets)
        
        #Extract danger targets
        # Create bomb field indicating which tiles are dangerous
        bomb_field = np.zeros(field.shape)
        for j, bomb in enumerate(bomb_pos):
            # risk highest (=4), when bombtimer lowest (=1)
            #if bomb_timer[j] == 0:
            #    continue
            danger = 3 # - bomb_timer[j]
            bomb_field[bomb[0], bomb[1]] = danger
            for i in range(1, 4):
                if field[bomb[0]+i, bomb[1]] != -1:
                    bomb_field[bomb[0]+i, bomb[1]] = danger
                else: break
            for i in range(1, 4):
                if field[bomb[0]-i, bomb[1]] != -1:
                    bomb_field[bomb[0]-i, bomb[1]] = danger
                else: break
            for i in range(1, 4):
                if field[bomb[0], bomb[1]+i] != -1:
                    bomb_field[bomb[0], bomb[1]+i] = danger
                else: break
            for i in range(1, 4):
                if field[bomb[0], bomb[1]-i] != -1:
                    bomb_field[bomb[0], bomb[1]-i] = danger
                else: break
        #print('bomb_field:',bomb_field.T)
        #print('bomb_pos:',bomb_pos)
        #print('bomb_timer',bomb_timer)
        

        #
        # Generate Features (interpret 1 as 'good' direction and 0 as 'bad' direction)
        #
        # Use now 1D array with four entries, with the entries representing ('oben','unten','rechts','links')
        
        #print('self_pos:',self_pos)
        x = self_pos[0]
        y = self_pos[1]
        #neighbors = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
        neighbors = [[x + 1, y], [x - 1, y], [x, y + 1], [x, y - 1]]
        # Regarding the crates
        d1, crate_dist, best_crate, f1 = self.look_for_targets(free_space_crates, (x,y), crate_targets)
        #print('best_crate:',best_crate)
        #print('d1:',d1,' crate_dist=',crate_dist)
        # crate_feature = np.array([0,0,0,0]) # if d = None, then crate_feature will remain like this
        # if d1 == (x, y + 1): crate_feature = np.array([0,0,1,0]) # direction = RIGHT # not 'DOWN'
        # if d1 == (x, y - 1): crate_feature = np.array([0,0,0,1]) # direction = LEFT # not 'UP'
        # if d1 == (x + 1, y): crate_feature = np.array([1,0,0,0]) # direction = DOWN # not 'RIGHT'
        # if d1 == (x - 1, y): crate_feature = np.array([0,1,0,0]) # direction = UP # not 'LEFT'
        #if d1 is None: crate_feature = np.array([0,0,0,0]) # direction = None, no crate present
        #print('crate_feature',crate_feature)
        crate_feature = np.all((np.array(d1) == neighbors), axis=1).astype(np.int32)
        # Stop recommending walking, if next to crate
        if crate_dist == 1:
            crate_feature = np.zeros((4))
        #assert(np.all(crate_feature == crate_feature2))
        
        # Regarding the coins
        d2, coin_dist, best_coin, f2 = self.look_for_targets(free_space, (x,y), coin_targets)
        #print('best_coin=', best_coin)
        #print('d2:',d2,' coin_dist=',coin_dist)
        # coin_feature = np.array([0,0,0,0]) # if d = None, then coin_feature will remain like this
        # if d2 == (x, y + 1): coin_feature = np.array([0,0,1,0]) # direction = RIGHT # not 'DOWN'
        # if d2 == (x, y - 1): coin_feature = np.array([0,0,0,1]) # direction = LEFT # not 'UP'
        # if d2 == (x + 1, y): coin_feature = np.array([1,0,0,0]) # direction = DOWN # not 'RIGHT'
        # if d2 == (x - 1, y): coin_feature = np.array([0,1,0,0]) # direction = UP # not 'LEFT'
        #if d2 is None: coin_feature = np.array([0,0,0,0]) # direction = None, no coin present
        #print('coin_feature',coin_feature)
        if f2 == True: 
            coin_feature = np.all((np.array(d2) == neighbors), axis=1).astype(np.int32)
        else:
            coin_feature = np.zeros((4))
            coin_dist = 0
        #assert(np.all(coin_feature == coin_feature2))

        # Regarding the danger due to bombs and obstacles due to stone walls
        combined_field = field + bomb_field # combine danger zones and free spaces
        valid_fields = np.array(np.where(combined_field == 0)).T
        valid_targets = [*valid_fields]

        d3, safe_dist, best_safe, f3 = self.look_for_targets(free_space, (x,y), valid_targets)
        #print('safe_place=', best_safe)
        #print('d3:',d3,' safe_dist=',safe_dist)
        # safe_feature = np.array([0,0,0,0]) # if d = None, then coin_feature will remain like this
        # if d3 == (x, y + 1): safe_feature = np.array([0,0,1,0]) # direction = RIGHT # not 'DOWN'
        # if d3 == (x, y - 1): safe_feature = np.array([0,0,0,1]) # direction = LEFT # not 'UP'
        # if d3 == (x + 1, y): safe_feature = np.array([1,0,0,0]) # direction = DOWN # not 'RIGHT'
        # if d3 == (x - 1, y): safe_feature = np.array([0,1,0,0]) # direction = UP # not 'LEFT'
        #print("safe_feature:",safe_feature)
        safe_feature = np.all((np.array(d3) == neighbors), axis=1).astype(np.int32)
        # if it is hopeless, dont recommend a direction
        if f3 == False:
            safe_feature = np.zeros((4))
        #assert(np.all(safe_feature == safe_feature2))

        #print('others:',game_state['others'])
        #print('other_targets:',other_targets)
        other_tile, other_dist, best_other, f4 = self.look_for_targets(free_space_others, (x,y), other_targets)
        #print("best other: ", best_other)
        # print("other_tile:",other_tile,' other_dist=',other_dist)
        # other_feature = np.array([0,0,0,0]) # if other_tile = None, then other_feature will remain like this
        # if other_tile == (x, y + 1): other_feature = np.array([0,0,1,0]) # direction = RIGHT # not 'DOWN'
        # if other_tile == (x, y - 1): other_feature = np.array([0,0,0,1]) # direction = LEFT # not 'UP'
        # if other_tile == (x + 1, y): other_feature = np.array([1,0,0,0]) # direction = DOWN # not 'RIGHT'
        # if other_tile == (x - 1, y): other_feature = np.array([0,1,0,0]) # direction = UP # not 'LEFT'
        #print("other_feature:",other_feature)
        if f4 == True:
            other_feature = np.all((np.array(other_tile) == neighbors), axis=1).astype(np.int32)
            # stop walking on enemy next to you
            if other_dist == 1:
                other_feature = np.zeros((4))
        else:
            other_feature = np.zeros((4))
            other_dist = 0
        #assert(np.all(other_feature == other_feature2))


        # check if placing a bomb now is dangerous
        danger = 3
        bomb_field[x, y] = danger
        for i in range(1, 4):
            if field[x+i, y] != -1:
                bomb_field[x+i, y] = danger
            else: break
        for i in range(1, 4):
            if field[x-i, y] != -1:
                bomb_field[x-i, y] = danger
            else: break
        for i in range(1, 4):
            if field[x, y+i] != -1:
                bomb_field[x, y+i] = danger
            else: break
        for i in range(1, 4):
            if field[x, y-i] != -1:
                bomb_field[x, y-i] = danger
            else: break

        #print("BOMBFIELD: ", bomb_field)
        combined_field = field + bomb_field # combine danger zones and free spaces
        valid_fields = np.array(np.where(combined_field == 0)).T
        valid_targets = [*valid_fields]

        d5, safe_dist_2, best_safe_2, safe_available = self.look_for_targets(free_space, (x,y), valid_targets)
        #print("DIRECTION: ", d5)
        #print("SAFE_DISTA: ", safe_dist_2)
        #print("SAFE_FIELD: ", best_safe_2)
        #print("SAFE_AVAILABLE: ", safe_available)

        #print('------------ step:',game_state['step'])

        bomb_available = game_state['self'][2]
        X = np.concatenate([coin_feature,
                            #[coin_dist],
                            crate_feature,
                            [crate_dist],
                            safe_feature,
                            [safe_dist],
                            other_feature,
                            [other_dist],
                            [bomb_available],
                            [safe_available]
                            #[safe_available],
                            #[safe_available]
                        ])
        #print('X is ', X.astype(np.float32))
        #print('----------------------')
        
        #if np.any(X == None): print(X)
        return X.astype(np.float32)


    def reward_from_events(self, events):
        """
        Compute rewards from events.
        Currently a Wrapper for the utility function 'reward_from_events()'
        in case we want to try different rewards for different models.

        Parameters
        ----------
        game_state : dict
            game state to translate

        Returns
        -------
        np.array
            The feature vector.
        """
        game_rewards = {
            e.COIN_COLLECTED: 50, #20
            e.KILLED_OPPONENT: 100,
            e.CRATE_DESTROYED: 5, #2
            #e.COIN_FOUND: 5,
            e.GOT_KILLED: -100,
            e.KILLED_SELF: -100, #-150
            e.INVALID_ACTION: -6.0, #-3
            e.WAITED: -5.0, # -1
            e.BOMB_DROPPED: -1.0, #-1
            e.MOVED_LEFT: -1.0,
            e.MOVED_RIGHT: -1.0,
            e.MOVED_UP: -1.0,
            e.MOVED_DOWN: -1.0,
            #e.SURVIVED_ROUND: 1
            
            #PLACEHOLDER_EVENT: -.1  # idea: the custom event is bad
            #DIDNT_DODGE: -5,
            #HOPELESS_BOMB: -10
        }

        reward_sum = 0
        for event in events:
            if event in game_rewards:
                reward_sum += game_rewards[event]
        #self.agent_reference.logger.info(f"Awarded {reward_sum} for events {', '.join(events)}")
        return reward_sum
        #return reward_from_events(self.agent_reference, events)


    def actual_reward_from_events(self, events):
        """
        Compute rewards from events.
        Currently a Wrapper for the utility function 'reward_from_events()'
        in case we want to try different rewards for different models.

        Parameters
        ----------
        game_state : dict
            game state to translate

        Returns
        -------
        np.array
            The feature vector.
        """
        game_rewards = {
            e.COIN_COLLECTED: 1, 
            e.KILLED_OPPONENT: 5,
        }

        reward_sum = 0
        for event in events:
            if event in game_rewards:
                reward_sum += game_rewards[event]
        return reward_sum


    def look_for_targets(self, free_space, start, targets, logger=None):
        """Find direction of closest target that can be reached via free tiles.

        Performs a breadth-first search of the reachable free tiles until a target is encountered.
        If no target can be reached, the path that takes the agent closest to any target is chosen.

        Args:
            free_space: Boolean numpy array. True for free tiles and False for obstacles.
            start: the coordinate from which to begin the search.
            targets: list or array holding the coordinates of all target tiles.
            logger: optional logger object for debugging.
        Returns:
            coordinate of first step towards closest target or towards tile closest to any target.
        """
        if len(targets) == 0: return (None, 0, None, False)

        frontier = [start]
        parent_dict = {start: start}
        dist_so_far = {start: 0}
        best = start
        best_dist = np.sum(np.abs(np.subtract(targets, start)), axis=1).min()

        target_reached = False

        while len(frontier) > 0:
            current = frontier.pop(0)
            # Find distance from current position to all targets, track closest
            d = np.sum(np.abs(np.subtract(targets, current)), axis=1).min()
            if d + dist_so_far[current] <= best_dist:
                best = current
                best_dist = d + dist_so_far[current]
            if d == 0:
                # Found path to a target's exact position, mission accomplished!
                best_dist = d + dist_so_far[current]
                best = current
                target_reached = True
                break
            # Add unexplored free neighboring tiles to the queue in a random order
            x, y = current
            neighbors = [(x, y) for (x, y) in [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)] if free_space[x, y]]
            #print(frontier)
            #if neighbors == []: print("NEIGHBORS: ", neighbors)
            shuffle(neighbors)
            for neighbor in neighbors:
                if neighbor not in parent_dict:
                    frontier.append(neighbor)
                    parent_dict[neighbor] = current
                    dist_so_far[neighbor] = dist_so_far[current] + 1
        if logger: logger.debug(f'Suitable target found at {best}')
        # Determine the first step towards the best found target tile
        #print('best:',best)
        current = best
        while True:
            if parent_dict[current] == start: return (current,best_dist,best, target_reached) # best_dist
            current = parent_dict[current]