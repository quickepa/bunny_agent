import numpy as np
from typing import List
from collections import namedtuple
import scipy.linalg as scl

import events as e

ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward'))

# Events
PLACEHOLDER_EVENT = "PLACEHOLDER"

def nstepTD(R, Q_tplus, n, gamma):
    M = len(R)
    if M <= n:
        print("WARNING: M<=n!!!")
        n = np.min(np.array([M, n]))
    gamma_ = np.power(gamma, np.arange(0, n))
    row = np.pad(gamma_, (0, M-n), 'constant')
    col = np.zeros((M))
    col[0] = 1
    gamma_ = scl.toeplitz(col, row)
    R_ = np.repeat(np.reshape(R, (1, M)), M, axis = 0)
    R_nstep=np.diag(np.dot(R_, gamma_.T))
    Q_n = np.concatenate([np.max(Q_tplus, axis=1)[n:], np.zeros(n)])
    y = R_nstep + np.power(gamma, n)*Q_n
    return y


def state_to_features(game_state: dict):
    # @TODO: Standardize by recording 2000 games and computing mean and std

    # This is the dict before the game begins and after it ends
    if game_state is None:
        return None

    # Add agents position, so he can figure out, where he stands relative to other stuff
    self_pos = np.array(game_state['self'][3]) # 2

    # Add just possible stepping fields.
    oben = np.array([ game_state['field'][self_pos[0], self_pos[1]+1] ])
    unten = np.array([ game_state['field'][self_pos[0], self_pos[1]-1] ])
    rechts = np.array([ game_state['field'][self_pos[0]+1, self_pos[1]] ])
    links = np.array([ game_state['field'][self_pos[0]-1, self_pos[1]] ])
    umgebung = np.concatenate([oben, unten, rechts, links])

    # Add coin positions, so agent can find them
    coin_pos = np.array(game_state['coins']).flatten()
    coin_pos = np.pad(coin_pos, (0, 18-coin_pos.shape[0]), 'constant')
    coin_pos = np.reshape(coin_pos, (9,2))

    # Get position and distance of closest coin 
    #coin_distance = np.linalg.norm(coin_pos_np-self_pos, axis=1)
    coin_distances = np.sum(np.abs(coin_pos-self_pos), axis=1)
    coin_distances[coin_pos[:, 0] == 0] = 100
    closest_coin_pos = coin_pos[np.argmin(coin_distances), :]
    closest_coin_dist = np.min(coin_distances)

    # Add Bomb positions and timers, so he can figure out to avoid them
    bombs = np.array([[a, b, c] for (a, b), c in game_state['bombs']]).flatten()
    bombs = np.pad(bombs, (0, 12-len(bombs)), 'constant')# 12=3x4
    bomb_pos = np.reshape(bombs, (4, 3))[:, 0:2]
    bomb_distances = np.sum(np.abs(bomb_pos-self_pos), axis=1)
    bomb_distances[bomb_pos[:, 0] == 0] = -1
    bomb_timers = np.reshape(bombs, (4, 3))[:, 2]

    # For normalization we need the mean position of things
    mean_pos = np.array([16.0, 16.0])/2.0
    self_pos = self_pos - mean_pos
    umgebung = umgebung + 0.5
    coin_pos = (coin_pos - mean_pos).flatten()
    coin_distances = coin_distances - (29 + 1)/ 2
    closest_coin_pos = closest_coin_pos - mean_pos
    closest_coin_dist = closest_coin_dist - (29 + 1)/ 2
    bomb_pos = (bomb_pos - mean_pos).flatten()
    bomb_timers = (bomb_timers - 2).flatten()

    X = np.concatenate([self_pos, closest_coin_pos, [closest_coin_dist], umgebung])
    #X = np.concatenate([self_pos, coin_pos, coin_distances, umgebung])

    return X

def state_to_features2(game_state: dict):# -> np.array:
    """
    *This is not a required function, but an idea to structure your code.*

    Converts the game state to the input of your model, i.e.
    a feature vector.

    You can find out about the state of the game environment via game_state,
    which is a dictionary. Consult 'get_state_for_agent' in environment.py to see
    what it contains.

    :param game_state:  A dictionary describing the current game board.
    :return: np.array
    """
    # @TODO: Decide on what features to use and implement the conversion s->X

    # This is the dict before the game begins and after it ends
    if game_state is None:
        return None

    # For normalization we need the mean position of things
    mean_pos = np.array([16.0, 16.0])/2.0
    # Add agents position, so he can figure out, where he stands relative to other stuff
    self_pos = np.array(game_state['self'][3])# 2
    # Add the field, so agents knows where walls are
    field = game_state['field'].flatten() - 0.5 # 289
    # Add just possible stepping fields.
    oben = np.array([ game_state['field'][self_pos[0], self_pos[1]+1] ]) + 0.5
    unten = np.array([ game_state['field'][self_pos[0], self_pos[1]-1] ]) + 0.5
    rechts = np.array([ game_state['field'][self_pos[0]+1, self_pos[1]] ]) + 0.5
    links = np.array([ game_state['field'][self_pos[0]-1, self_pos[1]] ]) + 0.5

    # Add coin positions, so agent can find them
    coin_pos = (np.array(game_state['coins']) - mean_pos)
    coin_pos = coin_pos.flatten()
    coin_pos = np.pad(coin_pos, (0, 18-len(coin_pos)), 'constant') # 18
    # Add distance from coins
    coin_pos_np = np.array(game_state['coins'])#np.reshape(coins_pos, (9, 2))
    #coin_distance = (coin_pos_np-self_pos).flatten()
    #coin_distance = np.linalg.norm(coin_pos_np-self_pos, axis=1)
    coin_distance = np.sum(np.abs(coin_pos_np-self_pos), axis=1)
    coin_distance[coin_pos_np[:, 0] == 0] = 100
    closest_coin = coin_pos_np[np.argmin(coin_distance), :] - mean_pos
    #print(coin_distance.shape)
    # Add Bomb positions and timers, so he can figure out to avoid them
    bombs = np.array([[a, b, c] for (a, b), c in game_state['bombs']]).flatten()
    bombs = np.pad(bombs, (0, 12-len(bombs)), 'constant')# 12=3x4
    bomb_pos = (np.reshape(bombs, (4, 3))[:, 0:2] - mean_pos).flatten()
    bombs_distance = np.sum(np.abs(np.reshape(bombs, (4, 3))[:, 0:2]-self_pos), axis=1)
    bombs_timers = np.reshape(bombs, (4, 3))[:, 2]

    self_pos = np.array(game_state['self'][3]) - mean_pos# 2
    X = np.concatenate([oben, unten, rechts, links, coin_pos, self_pos, bomb_pos])
    #X = np.concatenate([field, coin_pos, self_pos, bombs])
    #X = np.concatenate([self_pos, oben, unten, rechts, links, coin_pos, bombs])
    #X = np.concatenate([self_pos, oben, unten, rechts, links, coin_distance, bombs_distance, bombs_timers])
    #X = np.concatenate([self_pos, oben, unten, rechts, links, coin_distance])
    #X = np.concatenate([self_pos, oben, unten, rechts, links, closest_coin])
    return X


def state_to_features_multiply(self, game_state: dict):
    """
    This conversion is the one that multiply_data uses.

    Converts the game state to the input of your model, i.e.
    a feature vector.

    You can find out about the state of the game environment via game_state,
    which is a dictionary. Consult 'get_state_for_agent' in environment.py to see
    what it contains.

    :param game_state:  A dictionary describing the current game board.
    :return: np.array
    """
    if game_state is None:
        return None

    # Add agents position, so he can figure out, where he stands relative to other stuff
    self_pos = np.array(game_state['self'][3]) # 2

    # Add just possible stepping fields.
    unten = np.array([ game_state['field'][self_pos[0], self_pos[1]+1] ])
    oben = np.array([ game_state['field'][self_pos[0], self_pos[1]-1] ])
    rechts = np.array([ game_state['field'][self_pos[0]+1, self_pos[1]] ])
    links = np.array([ game_state['field'][self_pos[0]-1, self_pos[1]] ])
    umgebung = np.concatenate([unten, oben, rechts, links])

    # Add coin positions, so agent can find them
    coin_pos = np.array(game_state['coins']).flatten()
    coin_pos = np.pad(coin_pos, (0, 18-coin_pos.shape[0]), 'constant')
    coin_pos = np.reshape(coin_pos, (9,2))
    #print('coin_pos:',coin_pos)

    # Get position and distance of closest coin 
    #coin_distance = np.linalg.norm(coin_pos_np-self_pos, axis=1)
    coin_distances = np.sum(np.abs(coin_pos-self_pos), axis=1)
    coin_distances[coin_pos[:, 0] == 0] = 100 # set to more than max distance
    #print('coin_distances:',coin_distances)
    closest_coin_pos = coin_pos[np.argmin(coin_distances), :]
    closest_coin_dist = np.min(coin_distances)
    #print('closest_coin_pos',closest_coin_pos)

    # Add Bomb positions and timers, so he can figure out to avoid them
    bombs = np.array([[a, b, c] for (a, b), c in game_state['bombs']]).flatten()
    bombs = np.pad(bombs, (0, 12-len(bombs)), 'constant')# 12=3x4
    bomb_pos = np.reshape(bombs, (4, 3))[:, 0:2]
    bomb_distances = np.sum(np.abs(bomb_pos-self_pos), axis=1)
    bomb_distances[bomb_pos[:, 0] == 0] = -1
    bomb_timers = np.reshape(bombs, (4, 3))[:, 2]

    # For normalization we need the mean position of things
    mean_pos = np.array([16.0, 16.0])/2.0
    self_pos = self_pos - mean_pos
    umgebung = umgebung + 0.5
    closest_coin_pos = closest_coin_pos - mean_pos
    closest_coin_dist = closest_coin_dist - (29 + 1)/ 2
    bomb_pos = (bomb_pos - mean_pos).flatten()
    bomb_timers = (bomb_timers - 2).flatten()

    #print("bomb_pos:",bomb_pos)
    #print("bomb_dist:",bomb_distances)
    #print("bomb_timers:",bomb_timers)
    X = np.concatenate([self_pos, closest_coin_pos, bomb_pos, [closest_coin_dist], bomb_distances, bomb_timers, umgebung])

    #print("feature:",X)
    return X


def reward_from_events(self, events: List[str]):# -> int:
    """
    *This is not a required function, but an idea to structure your code.*

    Here you can modify the rewards your agent get so as to en/discourage
    certain behavior.
    """
    game_rewards = {
        e.COIN_COLLECTED: 1,
        e.KILLED_OPPONENT: 5,
        #e.KILLED_SELF: -20,
        #e.BOMB_DROPPED: -10,
        #e.INVALID_ACTION: -100.0,
        #e.MOVED_LEFT: 0.01,
        #e.MOVED_RIGHT: 0.01,
        #e.MOVED_UP: 0.01,
        #e.MOVED_DOWN: 0.01,
        #e.WAITED: -100.0,
        #e.SURVIVED_ROUND: 1
        
        #PLACEHOLDER_EVENT: -.1  # idea: the custom event is bad
    }

    reward_sum = 0
    for event in events:
        if event in game_rewards:
            reward_sum += game_rewards[event]
    self.logger.info(f"Awarded {reward_sum} for events {', '.join(events)}")
    return reward_sum


def mirror_matrices(X,mirror_horizontally = True):
    """
    This function mirrors mall matrices in an array of matrices on their respective 'middle axis'.
    The mirroring can go either horizontally or vertically. Note that the matrices should be square matrices.
    The second and third axis should be of the same size, the first axis of the input X gives the number of matrices to be mirrored.

    Parameters
    ----------
    X : np.ndarray
        Array containing many sqaure matrices that should be mirrored
    mirror_horizontally : boolean
        Whether to mirror horizontally or vertically (mirror horizontally means mirroring on a vertical line in horizontal direction)
    
    Returns
    -------
    np.ndarray
        Mirrored array of matrices
    """
    if mirror_horizontally:
        # vertical middle line fixed for square matrices of odd shape
        return np.pad(X,((0,0),(0,0),(X.shape[1]-1,0)),'reflect')[:,:,:X.shape[2]]
    else:
        # horizontal middle line fixed for square matrices of odd shape
        return np.pad(X,((0,0),(X.shape[1]-1,0),(0,0)),'reflect')[:,:X.shape[1],:]


def transpose_matrices(X):
    """
    This function transposes each matrix in an array of matrices X.

    Parameters
    ----------
    state : dict
        State on which to predict optimal action

    Returns
    -------
    np.ndarray
        Transposed array of matrices
    """
    return X.transpose((0,2,1))
    
    
def mirror_positions(v, mirror_horizontally=True):
    """
    This function mirrors positions in a position vector within our game board of fixed size 17x17.
    The mirroring can go either horizontally or vertically.

    Parameters
    ----------
    v : np.ndarray
        2D Array containing many positions that should be mirrored ( shape should be Nx2)
    mirror_horizontally : boolean
        Whether to mirror horizontally or vertically (mirror horizontally means mirroring on a vertical line in horizontal direction)
    
    Returns
    -------
    np.ndarray
        Mirrored array of positions
    """
    if mirror_horizontally:
        v_new = np.array([16 - v[:,0],v[:,1]]).T
        return np.where(v_new == 16,0,v_new) # Re-map values to 0, because if an entry is 0, it should stay 0 (bec. no coin present here)
    else:
        v_new = np.array([v[:,0],16 - v[:,1]]).T
        return np.where(v_new == 16,0,v_new) # because if entry is 0, it should stay 0 ( e.g. bec. no coin present here)


def transpose_positions(v):
    """
    This function transposes the positions in a position vector of shape Nx2.

    Parameters
    ----------
    v : np.ndarray
        N x 2 array containing positions

    Returns
    -------
    np.ndarray
        Array of transposed positions
    """
    return v[:,[1,0]]


def mirror_actions(v,idx_up,idx_right,idx_down,idx_left,mirror_horizontally=True):
    """
    This functinos mirrors all actions in a given array

    Parameters
    ----------
    v : np.array
        1D array containing positions
    idx : np.arrays
        Indices of specific actions

    Returns
    -------
    np.array
        Array of mirrored actions
    """
    w = v.copy()
    if mirror_horizontally:
        w[idx_right] = 3 # 'LEFT'
        w[idx_left] = 1 # 'RIGHT'
        return w
    else:
        w[idx_up] = 2 # 'DOWN'
        w[idx_down] = 0 # 'UP'
        return w


def transpose_actions(v,idx_up,idx_right,idx_down,idx_left):
    """
    This function transposes all actions.
    Input is a 1D vector containing all actions.

    Parameters
    ----------
    v : np.array
        array containing positions
    idx : np.arrays
        Indices of specific actions

    Returns
    -------
    np.array
        Array of transposed actions
    """
    w = v.copy()
    w[idx_up] = 3 # 'LEFT'
    w[idx_down] = 1 #'RIGHT'
    w[idx_left] = 0 # 'UP'
    w[idx_right] = 2 # 'DOWN'
    return w


def mirror_environment(X,mirror_horizontally=True):
    """
    This function mirrors the environment features of a set of states.

    Parameters
    ----------
    X : np.ndarray
        X is a Nx4 matrix, with each row containing ('unten','oben','rechts','links') i.e. the environment
    idx : np.arrays
        Indices of specific actions

    Returns
    -------
    np.ndarray
        Array of mirrored environments
    """
    if mirror_horizontally:
        return X[:,[1,0,2,3]] # exchange values of 'oben and 'unten'
    else:
        return X[:,[0,1,3,2]] # exchange values of 'rechts' and 'links'


def transpose_environment(X):
    """
    This function transposes the environment features of a set of states.

    Parameters
    ----------
    X : np.ndarray
        X is a Nx4 matrix, with each row containing ('unten','oben','rechts','links') i.e. the environment

    Returns
    -------
    np.ndarray
        Array of transposed environments
    """
    return X[:,[2,3,0,1]] # transpose all values ('oben' <-> 'links' and 'unten' <-> 'rechts')


def multiply_data(transitions):
    """
    This function multiplies the input data by a factor of 8 by rotating/mirroring all features.
    Here, transpositions and mirroring is used, but we arrive at the same states as if we used rotations

    Parameters
    ----------
    transitions : deque of 'Transition's
        Contains all transitions of an entire episode. 

    Returns
    -------
    np.ndarray
        Array of multiplied old_states
    np.array
        Array of multiplied actions
    np.ndarray
        Array of multiplied new_states
    np.array
        Array of multiplied rewards
    """
    #
    # Extract data and transform into numpy arrays:
    #
    old_states, actions, new_states, rewards = zip(*transitions)
    old_states = np.array(old_states)
    actions = np.array(actions) # np.array(actions,dtype='<U5')
    #new_states = np.array(new_states,dtype='object')
    rewards = np.array(rewards)
    #print("old_states shape:",old_states.shape)
    #print("new_states shape:",new_states.shape)
    #print("actions shape:",actions.shape)
    #print("rewards shape:",rewards.shape)
    
    #
    # Extract those features representing fixed position coordinates (index 0-12) and turn/mirror them 8 times
    #
    mean_pos = np.array([16.0, 16.0])/2.0 # for de-centralization
    positions_A = old_states[:,:12].reshape(old_states.shape[0]*6,2) + mean_pos # reshape s.t. we have 2-tuples representing the coordinates and add mean_pos for de-centralization
    positions_H = transpose_positions(positions_A)
    positions_E = mirror_positions(positions_A)
    positions_G = mirror_positions(positions_A,mirror_horizontally=False)
    positions_B = transpose_positions(positions_E)
    positions_D = transpose_positions(positions_G)
    positions_C = mirror_positions(positions_G)
    positions_F = mirror_positions(positions_B)
    #print("positions_A shape:",positions_A.shape)
    #print("positions_G:",positions_G)

    # Re-Centralize positions again
    positions_A = positions_A - mean_pos
    positions_H = positions_H - mean_pos
    positions_E = positions_E - mean_pos
    positions_G = positions_G - mean_pos
    positions_B = positions_B - mean_pos
    positions_D = positions_D - mean_pos
    positions_C = positions_C - mean_pos
    positions_F = positions_F - mean_pos
    
    #
    # Extract those features that are representing the 'environment':
    #
    environment_A = old_states[:,21:]
    environment_H = transpose_environment(environment_A)
    environment_E = mirror_environment(environment_A)
    environment_G = mirror_environment(environment_A,mirror_horizontally=False)
    environment_B = transpose_environment(environment_E)
    environment_D = transpose_environment(environment_G)
    environment_C = mirror_environment(environment_G)
    environment_F = mirror_environment(environment_B)
    #print('environment_A:',environment_A)
    #print('environment_H:',environment_H)
    
    #
    # Combine the multiplied data again into states (adapt if more agents present in game -> more relevant positions?)
    #
    # TODO: add also bomb timers and other stuff here to the respective old_states matrix 
    old_states_A = np.hstack((positions_A.reshape(old_states.shape[0],12),old_states[:,12:21],environment_A))
    old_states_B = np.hstack((positions_B.reshape(old_states.shape[0],12),old_states[:,12:21],environment_B))
    old_states_C = np.hstack((positions_C.reshape(old_states.shape[0],12),old_states[:,12:21],environment_C))
    old_states_D = np.hstack((positions_D.reshape(old_states.shape[0],12),old_states[:,12:21],environment_D))
    old_states_E = np.hstack((positions_E.reshape(old_states.shape[0],12),old_states[:,12:21],environment_E))
    old_states_F = np.hstack((positions_F.reshape(old_states.shape[0],12),old_states[:,12:21],environment_F))
    old_states_G = np.hstack((positions_G.reshape(old_states.shape[0],12),old_states[:,12:21],environment_G))
    old_states_H = np.hstack((positions_H.reshape(old_states.shape[0],12),old_states[:,12:21],environment_H))
    #print('old_States A shape',old_states_A.shape)
    #print('old_States H shape',old_states_H.shape)

    #
    # Now rebuild arrays multiplied_old_states & multiplied_new_states 
    #

    multiplied_old_states = np.vstack((old_states_A,old_states_B,old_states_C,old_states_D,\
                                        old_states_E,old_states_F,old_states_G,old_states_H))
    # Now use that old and new states are almost the same to construct new_states (but keep in mind that new_states is not even used for model fitting)
    new_states_A = list(old_states_A[1:,:])
    new_states_A.append(None)
    new_states_B = list(old_states_B[1:,:])
    new_states_B.append(None)
    new_states_C = list(old_states_C[1:,:])
    new_states_C.append(None)
    new_states_D = list(old_states_D[1:,:])
    new_states_D.append(None)
    new_states_E = list(old_states_E[1:,:])
    new_states_E.append(None)
    new_states_F = list(old_states_F[1:,:])
    new_states_F.append(None)
    new_states_G = list(old_states_G[1:,:])
    new_states_G.append(None)
    new_states_H = list(old_states_H[1:,:])
    new_states_H.append(None)
    multiplied_new_states = np.vstack((np.array(new_states_A,dtype='object'),np.array(new_states_B,dtype='object'),\
                                    np.array(new_states_C,dtype='object'),np.array(new_states_D,dtype='object'),\
                                    np.array(new_states_E,dtype='object'),np.array(new_states_F,dtype='object'),\
                                    np.array(new_states_G,dtype='object'),np.array(new_states_H,dtype='object'))).flatten()

    #print('multiplied_old_states shape',multiplied_old_states.shape) 
    #print('multiplied_new_states',multiplied_new_states.shape,multiplied_new_states)
    
    #
    # Now multiply and adjust the actions
    #
    idx_up = np.argwhere(actions == 0).flatten() # 'UP'
    idx_right = np.argwhere(actions == 1).flatten() # 'RIGHT'
    idx_down = np.argwhere(actions == 2).flatten() # 'DOWN'
    idx_left = np.argwhere(actions == 3).flatten() # 'LEFT'

    actions_A = actions
    actions_H = transpose_actions(actions_A,idx_up,idx_right,idx_down,idx_left)
    actions_E = mirror_actions(actions_A,idx_up,idx_right,idx_down,idx_left)
    actions_G = mirror_actions(actions_A,idx_up,idx_right,idx_down,idx_left,mirror_horizontally=False)
    actions_B = transpose_actions(actions_E,idx_up,idx_right,idx_down,idx_left)
    actions_D = transpose_actions(actions_G,idx_up,idx_right,idx_down,idx_left)
    actions_C = mirror_actions(actions_G,idx_up,idx_right,idx_down,idx_left)
    actions_F = mirror_actions(actions_B,idx_up,idx_right,idx_down,idx_left)

    multiplied_actions = np.vstack((actions_A,actions_B,actions_C,actions_D,\
                                    actions_E,actions_F,actions_G,actions_H)).flatten()#.reshape(-1,1)
    #print('actions_A',actions_A)
    #print('Actions_G',actions_G)
    #print('actions all:',multiplied_actions)

    #
    # Multiply the rewards (no adjustment needed here as rewards are invariant to transpositions and mirroring)
    #
    multiplied_rewards = np.append(rewards,[rewards,rewards,rewards,rewards,rewards,rewards,rewards])#.reshape(-1,1)
    #print("rewards:",rewards)
    #print("all_rewards:",multiplied_rewards)

    #
    # Alternative output options
    #
    #Put all data together in one zip
    #multiplied_self_transitions = zip(multiplied_old_states, multiplied_actions, multiplied_new_states, multiplied_rewards)

    #Put all together as list of zips
    #multiplied_self_transitions = [zip(old_states_A,actions_A,new_states_A,rewards),\
    #                                zip(old_states_B,actions_B,new_states_B,rewards),\
    #                                zip(old_states_C,actions_C,new_states_C,rewards),\
    #                                zip(old_states_D,actions_D,new_states_D,rewards),\
    #                                zip(old_states_E,actions_E,new_states_E,rewards),\
    #                                zip(old_states_F,actions_F,new_states_F,rewards),\
    #                                zip(old_states_G,actions_G,new_states_G,rewards),\
    #                                zip(old_states_H,actions_H,new_states_H,rewards),]

    #return multiplied_self_transitions
    return multiplied_old_states, multiplied_actions, multiplied_new_states, multiplied_rewards