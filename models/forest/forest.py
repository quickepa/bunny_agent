import numpy as np
from collections import deque
from sklearn.ensemble import RandomForestRegressor

from ..IBrain import IBrain
from ..utility import state_to_features, reward_from_events, ACTIONS, Transition, nstepTD, multiply_data
from ..policy import softmax_policy, eps_greedy_policy

import events as e

class RandomForestBrain(IBrain):
    def __init__(self, agent_reference):
        #super.__init__()
        # keep reference to agent.self for logging and stuff
        self.agent_reference = agent_reference

        # Store a transition buffer per episode
        self.episode_memory = []
        self.current_episode= -1
        
        # Training runs
        self.n_training_rounds = 0

        # Error Analysis and feedback:
        self.rmse = np.empty((0))
        self.error_file = './analytics/forest/rmse_rewardshaping.npy'
        self.score = np.empty((0))
        self.score_file = './analytics/forest/score_rewardshaping.npy'

        # HYPERPARAMETERS
        # TODO: Find good values
        self.gamma = 0.9 # Discount for TD / SARSA
        self.n_steps = 5 # Number of steps for n-step TD

        self.eps = 0.0 # epsilon for eps_greed_policy()
        self.rho = 1.0 #0.001 # temperature for softmax_policy()

        self.training_frequency = 50
        
        self.params = {
            'n_estimators': 100,
            'max_depth': 10,
            'n_jobs': -1
        }

        # The random forest regressor
        self.forest = RandomForestRegressor(**self.params)


    def train(self):
        """
        Training method called in 'train.py' after each EPISODE in 'end_of_round()'.
        Operates on data recorded in transtions.
        We train a random forest from scratch every x episodes.

        Other possibilities to train:
            - offline
        Other things:
            - train a forest for each action?
        Returns
        -------

        """
        # train every x episodes i.e. when enough episodes are recorded
        if len(self.episode_memory) % self.training_frequency != 0:
            return

        # Fill up these arrays episode by episode after computing y
        n_features = len(self.episode_memory[0][0].state)
        X = np.empty((0, n_features))
        y = np.empty((0))
        R = np.empty((0))
        A = np.empty((0), dtype=np.int32)

        # Compute y's for every episode
        for episode in self.episode_memory:
            # First Transition is discarded in append_transition!!!
            # Retrieve lists from the Deque of tuples
            old_states, actions, new_states, rewards = zip(*episode)
            #old_states, actions, new_states, rewards = multiply_data(episode)
            
            # Format for numpy goodness
            X_tau = np.array(old_states)
            R_tau = np.array(rewards)
            A_tau = np.array(actions, dtype=np.int32)

            # Compute expected return = response 
            Q_tplus = self.estimate_all_actions(X_tau)
            y_tau = nstepTD(R_tau, Q_tplus, self.n_steps, self.gamma)
            # One-step TD
            #y_tau = R_tau + self.gamma * np.concatenate([ np.max(Q_tplus[1:], axis=1), [0]])

            # Add them to overall training batch
            X = np.concatenate( [X, X_tau] )
            y = np.concatenate( [y, y_tau] )
            A = np.concatenate( [A, A_tau] )
            R = np.concatenate( [R, [np.sum(R_tau)]] )
        print("Have ",len(X),"transitions for this training run.")
        # Compute current guess
        Q_t = self.estimate_state_action_pairs(X, A)
        # Compute residuals
        res = y - Q_t
        # Prior RMSE
        rmse = np.sqrt(np.sum(res*res) / len(res))
        self.rmse = np.append(self.rmse, rmse) 
        np.save(self.error_file, self.rmse)
        # Save mean score of this training epoche
        mean_score = np.mean(R)
        self.score = np.append(self.score, mean_score)
        np.save(self.score_file, self.score)
        # Fit the forest on all episodes
        XA = self.combine_feature_action_pairs(X, A)
        self.forest.fit(XA, y)

        # Housekeeping
        self.n_training_rounds += 1
        # Clear Episode Memory
        self.episode_memory = []
        self.current_episode = -1
        return


    def combine_feature_action_pairs(self, X, A):
        """
        Concatenate states and their corresponding actions.
        Actions are coded as 0,...,5 and will be one-hot-coded here.

        Parameters
        ----------
        X : np.array shape=(#States, #Features)
            State feature matrix
        A : np.array shape=(#States)
            Vector of Actions perfomed in states X

        Returns
        -------
        np.array shape=(#States)
            States with their ((o-h-c) action appended.
        """
        #idx = np.array([ACTIONS.index(a) for a in A])
        A_oh = self.get_one_hot(A, len(ACTIONS))
        return np.concatenate([X, A_oh], axis=1)
    

    def combine_with_every_action(self, X):
        """
        Concatenate each state in X with all possible actions.

        Parameters
        ----------
        X : np.array shape=(#States, #Features)
            State feature matrix
        A : np.array shape=(#States)
            Vector of Actions perfomed in states X

        Returns
        -------
        np.array shape=(#States)
            States with their ((o-h-c) action appended.
        """
        n_states = X.shape[0]
        A_oh = np.eye(len(ACTIONS))
        AAA = np.repeat(A_oh, np.repeat(n_states, len(ACTIONS)), axis=0).reshape((len(ACTIONS), n_states, len(ACTIONS)))
        XXX = np.tile(X, (len(ACTIONS), 1, 1))
        return np.concatenate([XXX, AAA], -1)


    def get_one_hot(self, targets, nb_classes):
        """
        Perform one-hot-coding on a vector of integer coded targets.
        
        Parameters
        ----------
        targets : np.array shape=(#Targets)
            Vector with integer-coded targets (actions in our case)
             to be one-hot-coded.
        nb_classes : int
            Number of classes to be coded

        Returns
        -------
        np.array shape=(??)
            One-hot-coded targets
        """
        res = np.eye(nb_classes)[np.array(targets).reshape(-1)]
        return res.reshape(list(targets.shape)+[nb_classes])


    def estimate_all_actions(self, X):
        """
        For state feature vector (row) in Feature matrix X, compute
        Q-value for all possible actions
        i.e. Q(x, a) for a in ACTIONS, x fixed
        If no model has been fitted yet, return vector of 0's.

        Parameters
        ----------
        X : np.array shape=(#States, #Features)
            State feature matrix

        Returns
        -------
        np.array shape=(#States, #Actions)
            Q estimates for all states and all possible actions
        """
        # if no model fitted yet, return 0s
        if self.n_training_rounds == 0:
            return np.zeros((len(X), len(ACTIONS)))
        # Combine each state with all actions
        Xa = self.combine_with_every_action(X)
        Xa = Xa.reshape((len(X)*len(ACTIONS), X.shape[1]+len(ACTIONS)))
        Q = self.forest.predict(Xa).reshape(len(X), len(ACTIONS))
        return Q
    

    def estimate_state_action_pairs(self, X, A):
        """
        For all state-actions-pairs, compute Q-values
        i.e. Q(x, a) for (x, a) in (X, A])
        If no model has been fitted yet, return vector of 0's.

        Parameters
        ----------
        X : np.array shape=(#States, #Features)
            State feature matrix
        A : np.array
            Actions (encoded as 0...5) which were performed in the corresponding states

        Returns
        -------
        np.array shape=(#States)
            Q estimates for all pairs (x, a) in (X, A)
        """
        # If no forest has not been fitted yet, return 0's
        if self.n_training_rounds == 0:
            return np.zeros( (len(X)) )
        # Compute estimates
        # @TODO
        XA = self.combine_feature_action_pairs(X, A)
        Q = self.forest.predict(XA)
        return Q


    def policy(self, state):
        """
        The policy that is called in 'callbacks.py' in 'act()'.
        Predict the Value function with current model and decide
        on an action according to some policy.

        Parameters
        ----------
        state : dict
            State on which to predict optimal action

        Returns
        -------
        String
            Optimal Action according to the policy
        """
        Q_actions = self.estimate_all_actions(self.state_to_features(state)[np.newaxis, :]).flatten()
        #next_action = softmax_policy(Q_actions, self.rho)
        #self.logger.info(f"The predicted action is {next_action}")
        #return eps_greedy_policy(Q_actions, self.eps)
        return softmax_policy(Q_actions, self.rho, return_probabilities=True) #next_action
        

    def append_transition(self, old_game_state, self_action, new_game_state, events):
        """
        Append new transition to the transtion buffer.
        Called in 'game_events_occurred()' and 'end_of_round()'.

        Parameters
        ----------
        old_game_state : dict
            The passed game state
        self_action : string
            The recorded action
        new_game_state : dict
            The game state that followed the action
        events : List[str]
            Events that occured during the transition
        """
        # A new episodes starts, so add a new queue to the memory
        if self_action == None:
            self.episode_memory.append(deque(maxlen=400))
            self.current_episode += 1
            return

        self.episode_memory[self.current_episode].append(
            (Transition(self.state_to_features(old_game_state), ACTIONS.index(self_action), self.state_to_features(new_game_state), self.reward_from_events(events))))


    def state_to_features(self, game_state: dict):
        """
        Translate a game state into features.
        Currently a Wrapper for the utility function 'state_to_features()'
        in case we want to try different features for different models. ->CHANGED it here!

        Parameters
        ----------
        game_state : dict
            game state to translate

        Returns
        -------
        np.array
            The feature vector.
        """
        #return state_to_features(game_state)
        #def state_to_features(game_state: dict):
        '''
        Here we transform the game states to features.
        Currently we involve the self position, closest coin, the environment around
        the self position and the bombs with their respective timers.
        '''
        # @TODO: Standardize by recording 2000 games and computing mean and std
        #print(game_state)
        # This is the dict before the game begins and after it ends
        if game_state is None:
            return None

        # Add agents position, so he can figure out, where he stands relative to other stuff
        self_pos = np.array(game_state['self'][3]) # 2

        # Add just possible stepping fields.
        unten = np.array([ game_state['field'][self_pos[0], self_pos[1]+1] ])
        oben = np.array([ game_state['field'][self_pos[0], self_pos[1]-1] ])
        rechts = np.array([ game_state['field'][self_pos[0]+1, self_pos[1]] ])
        links = np.array([ game_state['field'][self_pos[0]-1, self_pos[1]] ])
        umgebung = np.concatenate([unten, oben, rechts, links])

        # Add coin positions, so agent can find them
        coin_pos = np.array(game_state['coins']).flatten()
        coin_pos = np.pad(coin_pos, (0, 18-coin_pos.shape[0]), 'constant')
        coin_pos = np.reshape(coin_pos, (9,2))
        #print('coin_pos:',coin_pos)

        # Get position and distance of closest coin 
        #coin_distance = np.linalg.norm(coin_pos_np-self_pos, axis=1)
        coin_distances = np.sum(np.abs(coin_pos-self_pos), axis=1)
        coin_distances[coin_pos[:, 0] == 0] = 100 # set to more than max distance
        #print('coin_distances:',coin_distances)
        closest_coin_pos = coin_pos[np.argmin(coin_distances), :]
        closest_coin_dist = np.min(coin_distances)
        #print('closest_coin_pos',closest_coin_pos)

        # Add Bomb positions and timers, so he can figure out to avoid them
        bombs = np.array([[a, b, c] for (a, b), c in game_state['bombs']]).flatten()
        bombs = np.pad(bombs, (0, 12-len(bombs)), 'constant')# 12=3x4
        bomb_pos = np.reshape(bombs, (4, 3))[:, 0:2]
        bomb_distances = np.sum(np.abs(bomb_pos-self_pos), axis=1)
        bomb_distances[bomb_pos[:, 0] == 0] = -1
        bomb_timers = np.reshape(bombs, (4, 3))[:, 2]

        # For normalization we need the mean position of things
        mean_pos = np.array([16.0, 16.0])/2.0
        self_pos = self_pos - mean_pos
        umgebung = umgebung + 0.5
        closest_coin_pos = closest_coin_pos - mean_pos
        closest_coin_dist = closest_coin_dist - (29 + 1)/ 2
        bomb_pos = (bomb_pos - mean_pos).flatten()
        bomb_timers = (bomb_timers - 2).flatten()

        #print("bomb_pos:",bomb_pos)
        #print("bomb_dist:",bomb_distances)
        #print("bomb_timers:",bomb_timers)
        X = np.concatenate([self_pos, closest_coin_pos, bomb_pos, [closest_coin_dist], bomb_distances, bomb_timers, umgebung])

        #print("feature:",X)
        return X

    
    def reward_from_events(self, events):
        """
        Compute rewards from events.
        Currently a Wrapper for the utility function 'reward_from_events()'
        in case we want to try different rewards for different models.

        Parameters
        ----------
        game_state : dict
            game state to translate

        Returns
        -------
        np.array
            The feature vector.
        """
        return reward_from_events(self.agent_reference, events)

        #
        # Alternative rewards:
        #
        #game_rewards = {
        #    e.COIN_COLLECTED: 20, # chosen to be 1/5 times the rewards for killing an enemy (same ratio as initial values here)
        #    e.KILLED_OPPONENT: 100,#

        #    # Punish every action in order to force the agent to move to the coins quickly
        #    e.MOVED_UP: -1.,  
        #    e.MOVED_DOWN: -1.,
        #    e.MOVED_LEFT: -1.,
        #    e.MOVED_RIGHT: -1.,
        #    e.WAITED: -1.,
        #    e.INVALID_ACTION: -2.,
        #    e.BOMB_DROPPED: -1., # should be same as moving or waiting
        #    e.KILLED_SELF: -400. #, # punishment here maybe needs to be higher than 400*(punishment for any other action)
        #    #e.ALL_COINS_COLLECTED: 5
        #}
        #reward_sum = 0
        #for event in events:
        #    if event in game_rewards:
        #        reward_sum += game_rewards[event]
        #self.agent_reference.logger.info(f"Awarded {reward_sum} for events {', '.join(events)}")
        #return reward_sum