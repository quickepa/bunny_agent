import numpy as np
from collections import namedtuple, deque
import copy
import random
import scipy.linalg as scl
from sklearn.ensemble import GradientBoostingRegressor
import xgboost

from ..IBrain import IBrain
from ..utility import state_to_features, reward_from_events, ACTIONS, Transition
from ..policy import softmax_policy, eps_greedy_policy

class BoostingEnsemble:
    """
    Gradient Boosing Ensemble
    These will be used per action in the GBoostingBrain.
    """
    def __init__(self):
        # List of ensemble members and their weights (list of tuples)
        self.regressors = []
    
    def estimate(self, X):
        """
        Estimate the Q-value of each state by each ensemble member and return the weighted sum.
        There is one ensemble per action.
        
        Parameters
        ----------
        X : np.array shape=(#States, #Features)
            State feature matrix

        Returns
        -------
        np.array shape=(#States)
            Q estimates for all states given in X
        """
        estimates = np.array([weight*regressor.predict(X) for (weight, regressor) in self.regressors])
        #print("BOOSTING ENSEMBLE: ", estimates, " : ", type(estimates), " : ", estimates.shape )
        return np.sum(estimates, axis=0)

    def add_member(self, weight, regressor):
        """
        Add member and its to the ensemble
        
        Parameters
        ----------
        weight : float
            weight of member
        regressor : sklearn.ensemble.GradientBoostingRegressor or xgboost
            new ensemble member
        """
        self.regressors.append((weight, regressor))

class GBoostingBrain(IBrain):
    """
    Gradient Boosting Model
    """
    def __init__(self, agent_reference):
        #super.__init__()
        # keep reference to agent.self for logging and stuff
        self.agent_reference = agent_reference 
        
        # Transition Buffer stuff
        # TODO: Maybe do a list of transitions to hold
        # history of each episode?
        self.transition_buffer_size = 200000 # max buffer size
        self.n_transitions = 0 # number of transitions currently in the buffer
        self.transitions = deque(maxlen=self.transition_buffer_size)
        
        # Store a transition buffer per episode
        self.trajectories = []
        self.current_trajectory = -1

        # Training runs
        self.n_training_rounds = 0
        # Number of episodes played
        self.n_episodes_played = 0

        # Training batch sizes
        self.min_batch_size = 1
        self.batch_size = self.min_batch_size
        self.max_batch_size = 400
        
        # Error Analysis and feedback:
        self.rmse = np.empty((0, 6))
        self.error_file = './analytics/boosting/rmse.npy'

        # The ensemble(s) 
        self.ensembles = dict() # (map indexed by action)
        for action in ACTIONS:
            self.ensembles[action] = BoostingEnsemble()
        
        # HYPERPARAMETERS
        # TODO: Find good values
        self.gamma = 0.95 # Discount for TD / SARSA
        self.n_steps = 1 # Number of steps for n-step TD
        self.w_0 = 1.0 # starting weight for ensemble members
        self.mu = 0.1
        self.w_min = 0.01
        self.eps = 0.0 # epsilon for eps_greed_policy()
        self.rho = 1.0 # Temperatur for softmax_policy()

        # Params for regressor from library
        self.params = {
            'loss': 'ls',
            'learning_rate': 0.1,
            'n_estimators': 1, # EACH REGRESSOR IS JUST ONE BOOSTER
            'subsample': 1.0,
            'criterion': 'friedman_mse',
            'min_samples_split': 2,
            'min_samples_leaf': 1,
            'min_weight_fraction_leaf': 0.0, 
            'max_depth': 5,
            'min_impurity_decrease': 0.0,
            'min_impurity_split': None,
            'init': None, # Later this might be the estimator itself?
            'random_state': None,
            'max_features': None,
            'alpha': 0.9,
            'verbose': 0,
            'max_leaf_nodes': None,
            'warm_start': False,
            'validation_fraction': 0.1,
            'n_iter_no_change': None,
            'tol': 1e-4,
            'ccp_alpha': 0.0
        }

    
    def train(self):
        """
        Training method called in 'train.py' after each EPISODE in 'end_of_round()'.
        Operates on data recorded in transtions.
        We train one ensemble per action. The fit for ensemble A is done on all instances
        where action A was taken and on the corresponding residuals.

        Other possibilities to train:
            - train only on ensemble and encode actions into features
            - only train, if enough batch is big enough (not strictly after every episode)
        Other things:
            - Store transitions per action, so we dont have to sort them?
            - Store transitions per episode and train on multiple episodes, to fit on bigger batch
        """
        # Only train every x episodes i.e. when enough episodes are recorded
        if len(self.trajectories) % 5 != 0:
            return

        # Fill up these arrays episode by episode after computing y
        n_features = len(self.trajectories[0][0].state)
        X = np.empty((0, n_features))
        res = np.empty((0))
        A = np.empty((0))

        for trajectory in self.trajectories:
            # Get rid of first entry, since 'old_state' and 'action' are 'None'
            #self.transitions.popleft()
            # Retrieve lists from the Deque of tuples
            #print(trajectory)
            old_states, actions, new_states, rewards = zip(*trajectory)

            # Format for numpy goodness
            X_tau = np.array(old_states)
            R_tau = np.array(rewards)
            A_tau = np.array(actions)

            # Compute n-step TD
            y_tau = self.nstepTD(R_tau, X_tau, 10)
            # Compute current guess of overall reward
            Q_t = self.estimate_state_action_pairs(X_tau, A_tau)
            # Compute residuals => target response
            res_tau = y_tau-Q_t

            # Add them to overall training batch
            X = np.concatenate( [X, X_tau] )
            res = np.concatenate( [res, res_tau] )
            A = np.concatenate( [A, A_tau] )

        self.n_episodes_played += len(self.trajectories)
        self.trajectories = []
        self.current_trajectory = -1
        
        errors = np.zeros((1, 6))
        # fit new ensemble members on residuals and add them to respective ensemble
        for a, action in enumerate(ACTIONS):
            new_member = GradientBoostingRegressor(**self.params)
            mask = np.where(A == a)
            if mask[0].shape[0] == 0: # if this action has not been performed in this episode, skip
                continue
            Xa = X[mask]
            resa = res[mask]
            dmatrix = xgboost.DMatrix(Xa, resa)
            param = {'max_depth': 5}
            #evallist = [(dmatrix, 'action-{}'.format(action))]
            #new_member = xgboost.train(param, dmatrix, evals=evallist, verbose_eval=True)
            new_member = xgboost.train(param, dmatrix)#, evals=evallist, verbose_eval=True)
            
            #new_member.fit(Xa, resa)
            #if action not in self.ensembles:
            #    self.ensembles[action] = BoostingEnsemble()
            weight = self.w_0 / (1+self.mu*self.n_training_rounds)
            self.ensembles[action].add_member(weight, copy.deepcopy(new_member))
            # Compute RMSE and log it
            rmse = np.sqrt(np.sum(resa*resa) / len(resa))
            errors[0, a] = rmse
            self.rmse = np.append(self.rmse, errors, axis=0)
            self.agent_reference.logger.info('Prior rmse for action {}: {}'.format(action, rmse))

        #self.rho /= 2
        #self.eps /= (1+self.n_training_rounds*0.04)
        self.n_training_rounds += 1

        np.save(self.error_file, self.rmse)

        return

    def estimate_state_action_pairs(self, X, A):
        """
        For all state-actions-pairs, compute Q-values
        i.e. Q(x, a) for (x, a) in (X, A])
        If no model has been fitted yet, return vector of 0's.

        Parameters
        ----------
        X : np.array shape=(#States, #Features)
            State feature matrix
        A : np.array
            Actions (encoded as 0...5) which were performed in the corresponding states

        Returns
        -------
        np.array shape=(#States)
            Q estimates for all pairs (x, a) in (X, A)
        """
        # Loop Version
        #return np.array([self.ensembles[a].estimate(X) for (x, a) in (X, A)])

        Q = np.zeros(len(X))
        for a, action in enumerate(ACTIONS):
            mask = np.where(A == a)
            # If there are no instances with this action, skip prediction
            if mask[0].shape[0] == 0:
                continue
            # If there is no ensemble for this action yet, put zeros
            if action not in self.ensembles:
                continue
            else:
                state_dmatrix = xgboost.DMatrix(X[mask])
                #Q[mask] = self.ensembles[action].estimate(X[mask])
                Q[mask] = self.ensembles[action].estimate(state_dmatrix)
        return Q


    def estimate_all_actions(self, X):
        """
        For state feature vector (row) in Feature matrix X, compute
        Q-value for all possible actions
        i.e. Q(x, a) for a in ACTIONS, x fixed
        If no model has been fitted yet, return vector of 0's.

        Parameters
        ----------
        X : np.array shape=(#States, #Features)
            State feature matrix

        Returns
        -------
        np.array shape=(#States, #Actions)
            Q estimates for all states and all possible actions
        """
        Q = np.zeros((len(X), len(ACTIONS)))
        for a, action in enumerate(ACTIONS):
            # If there is no ensemble for this action yet, put zeros
            if action not in self.ensembles:
                continue
            else:
                DX = xgboost.DMatrix(X)
                prediction = self.ensembles[action].estimate(DX)
                #print("ESTIMATE ALL ACTIONS: ", prediction, " : ", type(prediction), " : ", prediction.shape)
                Q[:, a] = prediction 
        #print("Q estimate all actions: ", Q)
        return Q


    def policy(self, state):
        """
        The policy that is called in 'callbacks.py' in 'act()'.
        Predict the Value function with current model and decide
        on an action according to some policy.

        Parameters
        ----------
        state : dict
            State on which to predict optimal action

        Returns
        -------
        String
            Optimal Action according to the policy
        """
        Q_actions = self.estimate_all_actions(self.state_to_features(state)[np.newaxis, :]).flatten()
        #return eps_greedy_policy(Q_actions, self.eps)
        return softmax_policy(Q_actions, self.rho)


    def append_transition(self, old_game_state, self_action, new_game_state, events):
        """
        Append new transition to the transtion buffer.
        Called in 'game_events_occurred()' and 'end_of_round()'.

        Parameters
        ----------
        old_game_state : dict
            The passed game state
        self_action : string
            The recorded action
        new_game_state : dict
            The game state that followed the action
        events : List[str]
            Events that occured during the transition
        """
        # New episode starts
        if self_action == None:
            self.trajectories.append(deque(maxlen=400))
            self.current_trajectory += 1
            return
        self.transitions.append(
            (Transition(self.state_to_features(old_game_state), ACTIONS.index(self_action), self.state_to_features(new_game_state), self.reward_from_events(events))))
        self.n_transitions +=1

        self.trajectories[self.current_trajectory].append(
            (Transition(self.state_to_features(old_game_state), ACTIONS.index(self_action), self.state_to_features(new_game_state), self.reward_from_events(events))))

        #print("Current Traj: ", self.current_trajectory)
        #print("Traj[0]", self.trajectories[self.current_trajectory][0].state)

    def state_to_features(self, game_state: dict):
        """
        Translate a game state into features.
        Currently a Wrapper for the utility function 'state_to_features()'
        in case we want to try different features for different models.

        Parameters
        ----------
        game_state : dict
            game state to translate

        Returns
        -------
        np.array
            The feature vector.
        """
        return state_to_features(game_state)
    

    def reward_from_events(self, events):
        """
        Compute rewards from events.
        Currently a Wrapper for the utility function 'reward_from_events()'
        in case we want to try different rewards for different models.

        Parameters
        ----------
        game_state : dict
            game state to translate

        Returns
        -------
        np.array
            The feature vector.
        """
        return reward_from_events(self.agent_reference, events)


    def nstepTD(self, R, X, n):
        M = len(R)
        if M <= n:
            print("WARNING: M<=n!!!")
            n = np.min(M, n)
        gamma_ = np.power(self.gamma, np.arange(0, n))
        row = np.pad(gamma_, (0, M-n), 'constant')
        col = np.zeros((M))
        col[0] = 1
        gamma_ = scl.toeplitz(col, row)
        R_ = np.repeat(np.reshape(R, (1, M)), M, axis = 0)
        R_nstep=np.diag(np.dot(R_, gamma_.T))
        Q_tplus = self.estimate_all_actions(X)
        Q_n = np.concatenate([np.max(Q_tplus, axis=1)[n:], np.zeros(n)])
        y = R_nstep + np.power(self.gamma, n)*Q_n
        return y

    """
    def init_transitions(self):
        '''
        Initialize the transition buffer as deque.
        Called in 'train.py' during 'setup_training()'
        '''
        self.n_transitions = 0
        self.transitions = deque(maxlen=self.transition_buffer_size)
    """