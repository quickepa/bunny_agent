import os
import pickle
import random

import numpy as np

from .random import RandomModel
from .boosting import GBoostingQEstimator
model_file_path_load = 'my-save-model.pt'

def setup(self):
    """
    Setup your code. This is called once when loading each agent.
    Make sure that you prepare everything such that act(...) can be called.

    When in training mode, the separate `setup_training` in train.py is called
    after this method. This separation allows you to share your trained agent
    with other students, without revealing your training code.

    In this example, our model is a set of probabilities over actions
    that are is independent of the game state.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    """
    # If a model is available, use it
    if os.path.isfile(model_file_path_load):
        self.logger.info("Loading model from saved state.")
        with open(model_file_path_load, "rb") as file:
            self.model = pickle.load(file)
    # If there is no model and there will be no training 
    # i.e. setup_training() not called, use RandomModel
    elif not self.train:
        self.logger.info("Setting up random model.")
        self.model = RandomModel() # TODO: This does no work, if you don't have models.py
    # If no model is available and training mode on,
    # handle this case in setup_training() ?
    else:
        self.logger.info("Setting up a new model.")
        self.model = GBoostingQEstimator(self) # TODO: This does no work, if you don't have models.py


def act(self, game_state: dict):# -> str:
    """
    Your agent should parse the input, think, and take a decision.
    When not in training mode, the maximum execution time for this method is 0.5s.

    :param self: The same object that is passed to all of your callbacks.
    :param game_state: The dictionary that describes everything on the board.
    :return: The action to take as a string.
    """

    self.logger.debug("Querying model for action.")
    return self.model.policy(game_state)