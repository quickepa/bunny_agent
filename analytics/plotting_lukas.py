import numpy as np
from matplotlib import pyplot as plt 

#
# RMSE plots
#

rmse_reference = np.load('rmse_reference_t50_n500.npy')
#rmse = np.load('./agent_code/bunny_agent/analytics/forest/rmse.npy')
#rmse_old = np.load('rmse_alt.npy')
#rmse_multi = np.load('rmse_multi.npy')
#rmse_t10_n100_nomulti = np.load("rmse_t10_n100_nomulti.npy")
#rmse_t80_n800_nomulti = np.load("rmse_t80_n800_nomulti.npy")
#rmse_t10_n100_multi = np.load("rmse_t10_n100_multi.npy")
#rmse_t10_n100_multi_stack = np.load("rmse_t10_n100_multi_stack.npy")
#rmse_t100_n1000_multi_stack = np.load("rmse_t100_n1000_multi_stack.npy")
#rmse = np.load('rmse_t500_n10000_multi.npy')
#rmse_trainedSelf= np.load('rmse_trainedSelf.npy')
rmse_rewardshaping = np.load('rmse_rewardshaping.npy')


fig = plt.figure(figsize=(10,5))

plt.title("RMSE Forest") 
plt.xlabel("Training Epoche") 
plt.ylabel(r"RMSE of $y_{\tau t}-Q(s_{\tau t},a_{\tau t})$")

plt.plot(rmse_reference,label='reference_t50_n500')
#plt.plot(rmse_t10_n100_nomulti,label='t10_n100_nomulti')
#plt.plot(rmse_t80_n800_nomulti,label='t80_n800_nomulti')
#plt.plot(rmse_t10_n100_multi,label='t10_n100_multi')
#plt.plot(rmse_t10_n100_multi_stack,label='rmse_t10_n100_multi_stack')
#plt.plot(rmse_t100_n1000_multi_stack,label='rmse_t100_n1000_multi_stack')
#plt.plot(rmse,label='rmse_t500_n10000_multi')
#plt.plot(rmse_multi)
#plt.plot(rmse_trainedSelf,label='rmse_t50_n1000_2nd part self trained')
plt.plot(rmse_rewardshaping,label='with rewardshaping')


plt.legend()
#fig.savefig('./agent_code/bunny_agent/analytics/forest/RMSE_Forest.jpg', bbox_inches='tight', dpi=150)
fig.savefig('RMSE_Forest_self_training.jpg', bbox_inches='tight', dpi=150)
plt.show()

#
# Score plots
#
score_reference = np.load('score_reference_t50_n500.npy')
score_rewardshaping = np.load('score_rewardshaping.npy')

plt.plot(score_reference,label='reference_t50_n500')

fig = plt.figure(figsize=(10,5))

plt.title("Score Forest") 
plt.xlabel("Training Epoche") 
plt.ylabel(r"score$")
