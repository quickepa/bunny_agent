import numpy as np
from matplotlib import pyplot as plt 

# Matplotlib standard colors
prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']

# Files
loss = np.load('./agent_code/bunny_agent/analytics/dql/loss.npy')
scores0 = np.load('./agent_code/bunny_agent/analytics/dql/scores_bunny_agent_0.npy')
scores1 = np.load('./agent_code/bunny_agent/analytics/dql/scores_bunny_agent_1.npy')
scores2 = np.load('./agent_code/bunny_agent/analytics/dql/scores_bunny_agent_2.npy')
scores3 = np.load('./agent_code/bunny_agent/analytics/dql/scores_bunny_agent_3.npy')

#
# Now plot the scores
#
bound = scores0.shape[0] // 100 * 100 # universal for all agents
survived_steps_max = np.max(np.concatenate([scores0[:,2],scores1[:,2],scores2[:,2],scores3[:,2]]))

fig, ax = plt.subplots(2,2,figsize=(24,14),sharex=True,sharey=True)
ax = ax.ravel()

ax[0].set_title("Scores - Agent 0")
#ax[0].set_xlabel("Episode")
ax[0].set_ylabel("Scores")
ax[0].plot(scores0[:, 0], 'o', markersize=1,color=colors[0],label='scores')
ax[0].plot(np.repeat(np.mean(scores0[:bound, 0].reshape(-1,100),axis=1),100), ls='-',color=colors[1],label='averaged scores')
#ax[0].axhline(0,0,max(scores0[:,0]),color='black',lw=1,label='score=0')
ax[0].plot(np.repeat(np.mean(scores0[:bound, 3].reshape(-1,100),axis=1),100), ls='-',color=colors[3],label='averaged points')
ax[0].tick_params(axis='y', labelcolor=colors[0])
ax[0].legend(loc='center left',fontsize=7)
ax0 = ax[0].twinx()
#ax0.plot(scores0[:, 2],color=colors[2],label='survived steps',lw=1)
ax0.plot(np.repeat(np.mean(scores0[:bound, 2].reshape(-1,100),axis=1),100), ls='-',color=colors[2],label='avg survived steps')
ax0.tick_params(axis='y', labelcolor=colors[2])
ax0.legend(loc='lower left',fontsize=7)
ax0.set_ylim(-10,survived_steps_max+10)

ax[1].set_title("Scores - Agent 1")
#ax[1].set_xlabel("Episode")
#ax[1].set_ylabel("Scores")
ax[1].plot(scores1[:, 0], 'o', markersize=1,color=colors[0],label='scores')
ax[1].plot(np.repeat(np.mean(scores1[:bound, 0].reshape(-1,100),axis=1),100), ls='-',color=colors[1],label='averaged scores')
#ax[1].axhline(0,0,max(scores1[:,0]),color='black',lw=1,label='score=0')
ax[1].plot(np.repeat(np.mean(scores1[:bound, 3].reshape(-1,100),axis=1),100), ls='-',color=colors[3],label='averaged points')
ax[1].tick_params(axis='y', labelcolor=colors[0])
ax[1].legend(loc='center left',fontsize=7)
ax1 = ax[1].twinx()
#ax1.plot(scores1[:, 2],color=colors[2],lw=1,ls='-',label='survived steps') # ,marker='o',markersize=1,ls=':'
ax1.plot(np.repeat(np.mean(scores1[:bound, 2].reshape(-1,100),axis=1),100), ls='-',color=colors[2],label='avg survived steps')
ax1.tick_params(axis='y', labelcolor=colors[2])
ax1.legend(loc='lower left',fontsize=7)
ax1.set_ylim(-10,survived_steps_max+10)

ax[2].set_title("Scores - Agent 2")
ax[2].set_xlabel("Episode")
ax[2].set_ylabel("Scores")
ax[2].plot(scores2[:, 0], 'o', markersize=1,color=colors[0],label='scores')
ax[2].plot(np.repeat(np.mean(scores2[:bound, 0].reshape(-1,100),axis=1),100), ls='-',color=colors[1],label='averaged scores')
#ax[2].axhline(0,0,max(scores2[:,0]),color='black',lw=1,label='score=0')
ax[2].plot(np.repeat(np.mean(scores2[:bound, 3].reshape(-1,100),axis=1),100), ls='-',color=colors[3],label='averaged points')
ax[2].tick_params(axis='y', labelcolor=colors[0])
ax[2].legend(loc='center left',fontsize=7)
ax2 = ax[2].twinx()
#ax2.plot(scores2[:, 2],color=colors[2],label='survived steps',lw=1)
ax2.plot(np.repeat(np.mean(scores2[:bound, 2].reshape(-1,100),axis=1),100), ls='-',color=colors[2],label='avg survived steps')
ax2.tick_params(axis='y', labelcolor=colors[2])
ax2.legend(loc='lower left',fontsize=7)
ax2.set_ylim(-10,survived_steps_max+10)

ax[3].set_title("Scores - Agent 3")
ax[3].set_xlabel("Episode")
#ax[3].set_ylabel("Scores")
ax[3].plot(scores3[:, 0], 'o', markersize=1,color=colors[0],label='scores')
ax[3].plot(np.repeat(np.mean(scores3[:bound, 0].reshape(-1,100),axis=1),100), ls='-',color=colors[1],label='averaged scores')
#ax[3].axhline(0,0,max(scores3[:,0]),color='black',lw=1,label='score=0')
ax[3].plot(np.repeat(np.mean(scores3[:bound, 3].reshape(-1,100),axis=1),100), ls='-',color=colors[3],label='averaged points')
ax[3].tick_params(axis='y', labelcolor=colors[0])
ax[3].legend(loc='center left',fontsize=7)
ax3 = ax[3].twinx()
#ax3.plot(scores3[:, 2],color=colors[2],label='survived steps',lw=1)
ax3.plot(np.repeat(np.mean(scores3[:bound, 2].reshape(-1,100),axis=1),100), ls='-',color=colors[2],label='avg survived steps')
ax3.tick_params(axis='y', labelcolor=colors[2])
ax3.legend(loc='lower left',fontsize=7)
ax3.set_ylim(-10,survived_steps_max+10)

fig.savefig('./agent_code/bunny_agent/analytics/dql/scoresMany3.jpg', bbox_inches='tight', dpi=150)
plt.show()


#
# Plot the loss of the model and hyperparameter epsilon
#
fig, ax = plt.subplots(1,2,figsize=(20,6))

ax[0].set_title(r"Loss of DQL") 
ax[0].set_xlabel("Step / Minibatch") 
ax[0].set_ylabel("MSE Loss")
ax[0].plot(loss,label='Loss')
ax[0].tick_params(axis='y', labelcolor=colors[0])
ax[0].legend()

#
# Plot epsilon
#
ax[1].set_title(r"Hyperparameter $\epsilon$ - Agent 0") 
ax[1].set_xlabel("Episode")
ax[1].plot(scores0[:, 1], color=colors[0], label=r'Hyperparamter $\epsilon$')
ax[1].tick_params(axis='y', labelcolor=colors[0])
ax[1].set_ylabel(r"Hyperparameter $\epsilon$")
ax[1].legend()

fig.savefig('./agent_code/bunny_agent/analytics/dql/loss_epsilon_Many3.jpg', bbox_inches='tight', dpi=150)
plt.show()


